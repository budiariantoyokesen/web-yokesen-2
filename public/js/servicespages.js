var target = "";
var current;
var widthScreen = $(window).width();
console.log(widthScreen);
var percentHeight = 0.22222;
var marginbottom = 690;
if (widthScreen < 576) {
    percentHeight = 0.37522;
}
$('.collapse').on('click', function () {
    var result = $('.collapse').css('margin-bottom', '0px');
    if (target === $(this).attr('target')) {
        setTimeout(function () {
            $(this).attr('class', 'icon-element--round--left--box--shadow--colorone collapse');
            $(target).hide();
            target = "";
        }, 500);
    } else {
        $('.media-collapse').hide();
        $('.icon-element--round--left--box--shadow--colorone--border').attr('class', 'icon-element--round--left--box--shadow--colorone collapse');
        $(this).attr('class', 'icon-element--round--left--box--shadow--colorone--border collapse');
        var heightTrg = $(this).height() + 10;
        var result = $('.collapse').css('margin-top', '0px');
        var this_ = this;
        setTimeout(function () {
            target = $(this_).attr('target');
            $(target).show();
            var offsettop = $(this_).offset();
            $(target).css("top", `${parseInt(offsettop.top) + heightTrg}px`);
            $(this_).css('margin-bottom', `${marginbottom}px`);
        }, 500);
    }

})
