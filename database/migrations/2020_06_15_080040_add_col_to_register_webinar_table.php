<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToRegisterWebinarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_webinar', function (Blueprint $table) {
            //
            $table->text('event_title_slug')->after('id');
            $table->tinyInteger('have_online_shop')->after('whatsapp_no');
            $table->text('online_shop_name')->nullable()->after('have_online_shop');
            $table->text('ecoomerce_platform')->nullable()->after('online_shop_name');
            $table->text('online_shop_others')->nullable()->after('ecoomerce_platform');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_webinar', function (Blueprint $table) {
            //
        });
    }
}
