<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('events');

        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->text('event_title');
            $table->text('event_title_slug');
            $table->string('event_content',7000)->nullable();
            $table->string('event_image_1',225)->nullable();
            $table->string('event_image_2',225)->nullable();
            $table->string('event_video',225)->nullable();
            $table->string('event_meta_description',225);
            $table->string('event_meta_keywords',225);
            $table->string('status',10);
            $table->string('created_by',60);
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
