<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => \UriLocalizer::localeFromRequest()], function () {
    Route::get('/', 'PageController@homeagency')->name('homeagency');
    Route::prefix('about')->group(function () {
        Route::get('yokesen', 'PageController@aboutyokesen')->name('about-yokesen');
        Route::get('yokesen-csr', 'PageController@aboutyokesencsr')->name('yokesen-csr');
    });
    Route::get('blogs', 'PageController@blogs')->name('blogs');
    Route::get('blog/{slug}', 'PageController@blog')->name('blog-detail');
    Route::get('digital-agency', 'PageController@digitalagency')->name('digital-agency');
    Route::get('technology', 'PageController@technology')->name('technology');
    Route::get('digital-consultant', 'PageController@digitalconsultant')->name('digital-consultant');
    Route::get('portfolios', 'PageController@portfolios')->name('portfolios');
    Route::get('portfolio/{portfolio_slug}', 'PageController@detailportfolio')->name('detail-portfolio');
    Route::get('careers', 'PageController@careers')->name('careers');
    Route::get('contact', 'PageController@contact')->name('contact');
    Route::get('yokesen-webinar', 'PageController@yokesenwebinar')->name('yokesen-webinar');
    Route::get('event/{event_title_slug}', 'PageController@event')->name('event-page');
    Route::get('yokesen-webinar/{event_title_slug}', 'PageController@yokesenwebinardetail')->name('webinar-page');
    Route::post('event-register', 'PageController@eventregister')->name('event-register');
    Route::get('event-register-thankyou', 'PageController@eventregisterthankyou')->name('event-register-thankyou');
    Route::get('sitemap.xml','PageController@sitemapxml')->name('sitemapxml');
});
Route::post('/post-comment-blog', 'PageController@postcommentblog')->name('postcommentblog');
Route::post('/send-message', 'PageController@sendmessage')->name('send-message');
Route::post('/register-yokesen-webinar', 'PageController@registeryokesenwebinar')->name('register-yokesen-webinar');
