CREATE TABLE `detail_services` ( 
`id` INT NOT NULL AUTO_INCREMENT ,
`services` VARCHAR(30) NOT NULL ,
`detail_services_name` VARCHAR(30) NOT NULL ,
`detail_service_description` VARCHAR(500) NOT NULL ,
`icon` VARCHAR(100) NULL ,
`status` VARCHAR(10) NOT NULL ,
`created_by` INT(3) NOT NULL ,
`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`));
