-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2020 at 05:22 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yokesen_db`
--

--
-- Truncate table before insert `detail_services`
--

TRUNCATE TABLE `detail_services`;
--
-- Dumping data for table `detail_services`
--

INSERT INTO `detail_services` (`id`, `services`, `detail_services_name`, `detail_service_description`, `icon`, `status`, `created_by`, `created_at`) VALUES
(1, 'Digital Agency', 'Social Media Agency', 'Implement 5 A’s of Social Media: Appearance, Appeal, Ask, Action and Advocacy.', 'ion-iphone', 'Active', 1, '2020-05-06 02:27:44'),
(2, 'Digital Agency', 'Digital Campaign', 'A plan for maximizing the business benefits of digital assets and technology-focused innitiatives.', 'ion-ios-world-outline', 'Active', 1, '2020-05-06 02:28:17'),
(3, 'Digital Agency', 'Digital Media Buying', 'Planning, mantaining and analyzing ads based on company’s segmentation.', 'ion-calculator', 'Active', 1, '2020-05-06 02:28:41'),
(4, 'Digital Agency', 'Digital Marketing Activation', 'Value creation, perspective distribution and servicing.', 'ion-android-share-alt', 'Active', 1, '2020-05-06 02:29:44'),
(5, 'Technology', 'Web Developer', 'Building, creating and maintaining website according to a company\'s specification', 'ion-android-laptop', 'Active', 1, '2020-05-06 02:41:11'),
(6, 'Technology', 'App Developer', 'The process of creating and mantaining applications, framework, or other software components.', 'ion-android-phone-portrait', 'Active', 1, '2020-05-06 02:41:23'),
(7, 'Technology', 'Software as a Service', '', 'ion-chatboxes', 'Active', 1, '2020-05-06 02:41:33'),
(8, 'Technology', 'Software as a Business', '', 'ion-briefcase', 'Active', 1, '2020-05-06 02:41:47'),
(9, 'Digital Consultant', 'Technology Adaptation', '', 'ion-code-working', 'Active', 1, '2020-05-06 02:42:46'),
(10, 'Digital Consultant', 'Technology Optimization', '', 'ion-ios-cog-outline', 'Active', 1, '2020-05-06 02:42:56'),
(11, 'Digital Consultant', 'Performance Management', '', 'ion-ios-speedometer-outline', 'Active', 1, '2020-05-06 02:43:08'),
(12, 'Digital Consultant', 'Digital Business Catalyst', '', 'ion-network', 'Active', 1, '2020-05-06 02:43:31');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
