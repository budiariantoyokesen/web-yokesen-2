-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2020 at 11:37 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yokesen_db`
--

--
-- Truncate table before insert `cms_menus`
--

TRUNCATE TABLE `cms_menus`;
--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(1, 'Portofolios', 'Route', 'AdminPortofoliosControllerGetIndex', NULL, 'fa fa-file-o', 0, 1, 0, 1, 1, '2020-04-28 21:27:37', NULL),
(2, 'Our Teams', 'Route', 'AdminOurTeamsControllerGetIndex', NULL, 'fa fa-user', 0, 1, 0, 1, 2, '2020-04-29 00:37:56', NULL),
(3, 'Our Partners', 'Route', 'AdminOurPartnersControllerGetIndex', NULL, 'fa fa-pencil', 0, 1, 0, 1, 3, '2020-04-29 03:58:32', NULL),
(4, 'Message', 'Route', 'AdminMessageControllerGetIndex', NULL, 'fa fa-envelope-square', 0, 1, 0, 1, 4, '2020-04-29 20:58:57', NULL),
(5, 'Register Webinar', 'Route', 'AdminRegisterWebinarControllerGetIndex', NULL, 'fa fa-file', 0, 1, 0, 1, 5, '2020-04-30 02:14:56', NULL),
(6, 'Blogs', 'Route', 'AdminBlogsControllerGetIndex', NULL, 'fa fa-file', 0, 1, 0, 1, 6, '2020-05-03 22:58:29', NULL);

--
-- Truncate table before insert `cms_moduls`
--

TRUNCATE TABLE `cms_moduls`;
--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2020-04-28 21:19:57', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2020-04-28 21:19:57', NULL, NULL),
(12, 'Portofolios', 'fa fa-file-o', 'Portofolios', 'portofolios', 'AdminPortofoliosController', 0, 0, '2020-04-28 21:27:37', NULL, NULL),
(13, 'Our Teams', 'fa fa-user', 'OurTeams', 'our_teams', 'AdminOurTeamsController', 0, 0, '2020-04-29 00:37:56', NULL, NULL),
(14, 'Our Partners', 'fa fa-pencil', 'OurPartners', 'our_partners', 'AdminOurPartnersController', 0, 0, '2020-04-29 03:58:32', NULL, NULL),
(15, 'Message', 'fa fa-envelope-square', 'Message', 'message', 'AdminMessageController', 0, 0, '2020-04-29 20:58:57', NULL, NULL),
(16, 'Register Webinar', 'fa fa-file', 'RegisterWebinar', 'register_webinar', 'AdminRegisterWebinarController', 0, 0, '2020-04-30 02:14:55', NULL, NULL),
(17, 'Blogs', 'fa fa-file', 'Blogs', 'blogs', 'AdminBlogsController', 0, 0, '2020-05-03 22:58:29', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
