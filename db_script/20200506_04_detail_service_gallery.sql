CREATE TABLE `detail_service_gallery` ( 
`id` INT NOT NULL AUTO_INCREMENT ,
`detail_services_id` INT(3) NOT NULL,
`image_url` TEXT NOT NULL ,
`status` VARCHAR(10) NOT NULL ,
`created_by` INT(3) NOT NULL ,
`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`));