ALTER TABLE `portofolios`
ADD `section_2_title` VARCHAR(200) NULL AFTER `tags_service`, 
ADD `section_2_text_1` TEXT NULL AFTER `section_2_title`, 
ADD `section_2_text_2` TEXT NULL AFTER `section_2_text_1`, 
ADD `section_2_text_3` TEXT NULL AFTER `section_2_text_2`, 
ADD `section_2_image_url` TEXT NULL AFTER `section_2_text_3`;


