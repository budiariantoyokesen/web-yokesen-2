@extends('templates.master')
@php
$noheaderimage=true;
@endphp
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding>
    <div class="container">
        <div class="row" data-padding="xstop">
            <div class="col-12 col-lg-7">
                <div class="sectiontitle-element">
                    <h1 data-aos="fade" class="big">{{trans('page.menu-careers')}}</h1>
                    <!-- <div data-aos="fade" data-aos-delay="200" data-isotope-filters>
                        <h3>Filter by</h3>
                        <ul>
                            <li data-filter="*" class="is-checked">All</li>
                            <li data-filter=".design">Design</li>
                            <li data-filter=".branding">Branding</li>
                            <li data-filter=".development">Development</li>
                            <li data-filter=".digital">Digital</li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Plans  -->
    <section data-padding="bottom" data-bg-bottom="">
        <div class="container">
            <div class="row" data-padding="xstop">
                <div data-aos="fade-up" class="col-12 col-lg-4">
                    <img class="img-responsive" src="{{url('/')}}/images/careers/career-yokesen-admin.png" alt="">
                </div>
                <div data-aos="fade-up" class="col-12 col-lg-4">
                    <img class="img-responsive" src="{{url('/')}}/images/careers/career-yokesen-agricultural-copywriter.png" alt="">
                </div>
                <div data-aos="fade-up" class="col-12 col-lg-4">
                    <img class="img-responsive" src="{{url('/')}}/images/careers/career-yokesen-designer.jpg" alt="">
                </div>
            </div>
        </div>
    </section>
    <!--  END Plans  -->

</div>
<!--  END Page Content  -->
@endsection