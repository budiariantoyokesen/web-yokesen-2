@extends('templates.master')
@php
$noheaderimage=true;
@endphp
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding>
    <div class="container">
        <div class="row" data-padding="xstop">
            <div class="col-12 col-lg-7">
                <div class="sectiontitle-element">
                    <h1 data-aos="fade" class="big">Portfolio</h1>
                    <div data-aos="fade" data-aos-delay="200" data-isotope-filters>
                        <h3>{{trans('page.filter_by')}}</h3>
                        <ul>
                            <li data-filter="*" class="is-checked">{{trans('page.all')}}</li>
                            <li data-filter=".digital-agency">{{trans('page.menu-services-digital-agency')}}</li>
                            <li data-filter=".technology">{{trans('page.menu-services-technology')}}</li>
                            <li data-filter=".digital-consultant">{{trans('page.menu-services-digital-consultant')}}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <section data-padding="bottom" data-isotope="load-simple" data-isotope-columns="three">
        <div class="container">
            <div class="row">
                @foreach($portfolios as $data )
                <!--  Single Project  -->
                <div class="square-element--project {{str_slug($data->tags_service)}}">
                    <div class="info">
                        <div class="image">
                            <img data-unveil data-src="{{url('/').'/'.$data->image_url}}" data-src-retina="{{url('/').'/'.$data->image_url}}" alt="{{!empty($data->alt_text)?$data->alt_text:$data->name.' - Yokesen - '.$data->tags_service}}">
                            <!-- <div class="icon--br--square--secondary"><i class="feather icon-plus"></i></div> -->
                        </div>
                        <div class="text">
                            <h6>{{$data->name}}</h6>
                            <p>{{$data->tags_service}}</p>
                        </div>
                    </div>
                    <a href="{{route('detail-portfolio',['portfolio_slug'=>$data->portfolio_slug])}}" class="link"></a>
                </div>
                <!--  END Single Project  -->
                @endforeach
            </div>
        </div>
    </section>
</div>
<!--  END Page Content  -->
@endsection