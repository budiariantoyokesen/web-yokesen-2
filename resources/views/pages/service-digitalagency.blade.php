@extends('templates.master')
@php
$noheaderimage=true;
@endphp

@section('cssonpage')
<style>
    .media-collapse {
        position: absolute;
        width: 100%;
    }
</style>

@endsection
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sectiontitle-element--center">
                    <span data-aos="fade-up" class="toptitle">{{trans('page.menu-services')}}</span>
                    <h1 data-aos="fade-up" data-aos-delay="100" class="big">{{trans('page.menu-services-digital-agency')}}</h1>
                    <p data-aos="fade-up" data-aos-delay="200">{{trans('page.services-digital-agency-title')}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Main Image  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="zoom-in" data-aos-duration="1000" class="row">
                <div class="col-12">
                    <div class="wrapimage-element">
                        <img data-unveil src="#" data-src="{{url('/')}}/images/services/digitalagency/Banner.png" data-src-retina="{{url('/')}}/images/services/digitalagency/Banner.png" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Main Image  -->
    <!--  Text  -->

    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade-up" class="row">
                <div class="col-12">
                    <div class="sectiontitle-element">
                        <h2>{{trans('page.what-we-did')}}</h2>
                        <!-- <p>Holy grail bandwidth stealth niche market freemium buyer traction. A/B testing paradigm shift stealth return on investment android startup user experience bootstrapping funding partnership agile development innovator network
                            effects. Beta series A financing buzz creative.</p> -->
                    </div>
                </div>
            </div>
            <div class="row" data-padding="smtop">
                @foreach($detailServices as $keys=>$data)
                <div class="col-12 col-md-6" data-margin="colbottom">
                    <div class="icon-element--round--left--box--shadow--colorone collapse" target="#collapse-{{$keys+1}}">
                        <div class="info">
                            <div class="icon"><i class="ilight feather {{!empty($data->icon)?$data->icon:'icon-activity'}}"></i></div>
                            <div class="text">
                                <h6>{{trans('page.'.$data->detail_services_name)}}</h6>
                                <p style="{{$keys==3?'height:58px;':''}}">{{trans('page.'.$data->detail_service_description)}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
    </section>
    <!--  END Text  -->
    <!--  Link Box  -->
    <section data-aos="fade" data-padding="xstop" class="aos-init aos-animate">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ctabox-element--center--round--light">
                        <div class="text">
                            <h2>{{trans('page.create-new-project')}}</h2>
                            <p>{{trans('page.to-do-what-you-want')}}</p>
                            <a href="mailto:contact@yokesen.com" class="btn--big--border--light--round" target="_self">{{trans('page.contact-now')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    @foreach($detailServices as $keys=>$data)
    <div class="media-collapse" id="collapse-{{$keys+1}}" style="display: none;" data-padding="smtop">
        <section data-aos="fade" data-padding="" class="aos-init aos-animate">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="sectiontitle-element--inline align-items-md-end">
                            <div class="wrapper">
                                <span class="toptitle">{{trans('page.'.$data->detail_services_name)}}</span>
                                <h2>{{trans('page.what-we-do')}}</h2>
                            </div>
                            <!--  Portfolio Nav  -->
                            <div class="wrapper portfolio-wrap">
                                <div class="navigation-element--border--round--light">
                                    <div class="navigation justify-content-md-end">
                                        <div class="nav--prev">
                                            <i class="feather icon-arrow-left"></i>
                                        </div>
                                        <div class="nav--next">
                                            <i class="feather icon-arrow-right"></i>
                                        </div>
                                        <!-- <a href="{{route('portfolios')}}" class="btn--big--round">{{trans('page.view-all')}}</a> -->
                                    </div>
                                </div>
                            </div>
                            <!--  END: Portfolio Nav  -->
                        </div>
                    </div>
                </div>
                <div class="row" data-padding="xstop">
                    <div class="col-12">
                        <div class="portfolio-carousel owl-carousel">
                            @if(count($data->galerry)>0)
                                @foreach($data->galerry as $image)
                                <!--  Portfolio Item  -->
                                <div class="square-element--project">
                                    <div class="info">
                                        <div class="image">
                                            <img class="owl-lazy" src="#" data-src="{{url('/').'/'.$image->image_url}}" data-src-retina="{{url('/').'/'.$image->image_url}}" alt="Yokesen, {{$data->detail_services_name.', '.$image->alt_text}}">
                                            <!-- <div class="icon--br--square--secondary">
                                                <i class="feather icon-plus"></i>
                                            </div> -->
                                        </div>
                                        <div class="text">
                                            <!-- <h6>Wording & Content creative</h6> -->
                                            <!-- <p>Branding</p> -->
                                        </div>
                                    </div>
                                    <!-- <a href="project.html" class="link"></a> -->
                                </div>
                                <!--  END: Portfolio Item  -->
                                @endforeach
                            @else
                            <div class="square-element--project">
                                <div class="info">
                                    <div class="image">
                                        <img class="owl-lazy" src="#" data-src="{{url('/')}}/img/case-1.jpg" data-src-retina="{{url('/')}}/img/case-1@2x.jpg" alt="">
                                        <!-- <div class="icon--br--square--secondary">
                                        <i class="feather icon-plus"></i>
                                    </div> -->
                                    </div>
                                    <div class="text">
                                        <!-- <h6>Quiz</h6> -->
                                        <!-- <p>Branding</p> -->
                                    </div>
                                </div>
                                <!-- <a href="project.html" class="link"></a> -->
                            </div>
                            <!--  END: Portfolio Item  -->
                            <div class="square-element--project">
                                <div class="info">
                                    <div class="image">
                                        <img class="owl-lazy" src="#" data-src="{{url('/')}}/img/case-2.jpg" data-src-retina="{{url('/')}}/img/case-2@2x.jpg" alt="">
                                        <!-- <div class="icon--br--square--secondary"><i class="feather icon-plus"></i></div> -->
                                    </div>
                                    <div class="text">
                                        <!-- <h6>Webinar</h6> -->
                                        <!-- <p>Design</p> -->
                                    </div>
                                </div>
                                <!-- <a href="project-2.html" class="link"></a> -->
                            </div>
                            <div class="square-element--project">
                                <div class="info">
                                    <div class="image">
                                        <img class="owl-lazy" src="#" data-src="{{url('/')}}/img/case-3.jpg" data-src-retina="{{url('/')}}/img/case-3@2x.jpg" alt="">
                                        <!-- <div class="icon--br--square--secondary"><i class="feather icon-plus"></i></div> -->
                                    </div>
                                    <div class="text">
                                        <!-- <h6>Share Campaign</h6> -->
                                        <!-- <p>Development</p> -->
                                    </div>
                                </div>
                                <!-- <a href="project.html" class="link"></a> -->
                            </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endforeach


</div>
<!--  END Page Content  -->
@endsection

@section('jsonpage')
<script src="{{url('/')}}/js/servicespages.js"></script>
@endsection