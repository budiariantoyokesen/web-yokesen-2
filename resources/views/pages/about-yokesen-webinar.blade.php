@extends('templates.master')
@php
$noheaderimage=true;
@endphp

@section('og-image','images/webinar/webinar.png')
@section('og-title','Start and Optimize Your Business Online')
@section('og-description','Yokesen held a webinar (web seminar) conducted online to support our program called Yok Bantu. The topic is "Starting and Optimizing your business online through Tokopedia". Start from how to open a shop in Tokopedia till optimize sales conversions.This webinar is intended for individuals who have just been laid off, SMEs with decreased turnover, individuals or businesses that have not yet sold online, individuals or businesses that have started selling online at Tokopedia but haven\'t getting result yet.')

@section('cssonpage')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding="top">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sectiontitle-element--center--full">
                    <h1 class="big">Webinar Yokesen - Tokopedia</h1>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Image Text  -->
    <section data-padding>
        <div class="container">
            <div data-aos="fade-up" class="row" data-padding="xstop">

                <div class="col-12">
                    <div class="wrapimage-element">
                        <img class="lazyload" data-unveil="" src="{{url('/')}}/images/webinar/yokesen_webinar.jpg" data-src="{{url('/')}}/images/webinar/yokesen_webinar.jpg" data-src-retina="{{url('/')}}/images/webinar/yokesen_webinar.jpg" alt="Yokesen,Yokesen Digital Marketing Activation, webinar, online seminar, online webinar" style="opacity: 1;">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade-up" class="row aos-init aos-animate">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="sectiontitle-element--full">
                        <h2>Start and Optimize Your Business Online</h2>
                        <p class="text-justify">Yokesen held a webinar (web seminar) conducted online to support our program called Yok Bantu. The topic is "Starting and Optimizing your business online through Tokopedia". Start from how to open a shop in Tokopedia till optimize sales conversions.</p>
                        <p class="text-justify">This webinar is intended for individuals who have just been laid off, SMEs with decreased turnover, individuals or businesses that have not yet sold online, individuals or businesses that have started selling online at Tokopedia but haven't getting result yet.</p>

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--  END Image Text  -->
    <!--  Info text  -->
    <section data-aos="fade-up" data-padding data-bg="grey">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="wrapimage-element--square">
                        <img data-unveil src="#" data-src="{{url('/')}}/images/webinar/webinar.png" data-src-retina="{{url('/')}}/images/webinar/webinar.png" alt="Yokesen,Yokesen Digital Marketing Activation, webinar, online seminar, online webinar">
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <div class="sectiontitle-element">
                        <form id="register-form">
                            <div class="basic-form--border--round--row">
                                <div class="field col-lg-12">
                                    <label for="name">Your Name <sup>*</sup></label>
                                    <input id="name" name="name" type="text" class="form-field" placeholder="You Name">
                                </div>
                                <div class="field col-lg-12">
                                    <label for="mail">Email Address <sup>*</sup></label>
                                    <!-- data-error="true" to set error field -->
                                    <input id="mail" name="mail" type="email" placeholder="Email" class="form-field" data-error="false">
                                </div>
                                <div class="field col-lg-12">
                                    <label for="whatsapp">Whatsapp Number<sup>*</sup></label>
                                    <!-- data-error="true" to set error field -->
                                    <input id="whatsapp" name="whatsapp" type="text" placeholder="Whatsapp Number" class="form-field" data-error="false">
                                </div>
                                <div class="field col-lg-12">
                                    <label for="link_tokopedia">Link Tokopedia</label>
                                    <!-- data-error="true" to set error field -->
                                    <input id="link_tokopedia" name="link_tokopedia" type="url" placeholder="Link Tokopedia" class="form-field" data-error="false">
                                </div>
                                <!-- data-active="false" is hidden, data-active="true" is visible -->
                                <div class="alert-wrap col-12" data-active="false">
                                    <p>For the error message see contact.php file</p>
                                </div>
                                <div class="field col-12">
                                    <input id="submit" type="submit" class="btn--big--round" value="Register">
                                </div>
                            </div>
                        </form>
                        <!-- <div class="navigation-element--border--round gallery-wrap">
                            <div class="navigation">
                                <div class="nav--prev">
                                    <i class="feather icon-arrow-left"></i>
                                </div>
                                <div class="nav--next">
                                    <i class="feather icon-arrow-right"></i>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Info text  -->
    <!--  Gallery Carousel  -->
    <!-- <section data-aos="fade" data-padding="bottom" data-bg-top="grey">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="gallery-carousel owl-carousel">
                        <div class="wrapimage-element--square">
                            <img data-unveil src="#" data-src="{{url('/')}}/img/about-gallery.jpg" data-src-retina="assets/img/about-gallery@2x.jpg" alt="">
                        </div>
                        <div class="wrapimage-element--square">
                            <img data-unveil src="#" data-src="{{url('/')}}/img/about-gallery-2.jpg" data-src-retina="assets/img/about-gallery-2@2x.jpg" alt="">
                        </div>
                        <div class="wrapimage-element--square">
                            <img data-unveil src="#" data-src="{{url('/')}}/img/about-gallery-3.jpg" data-src-retina="assets/img/about-gallery-3@2x.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--  END Gallery Carousel  -->

</div>
<!--  END Page Content  -->
@endsection


@section('jsonpage')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var submitContact = $('#register-form'),
        submitbutton = $('[type="submit"]'),
        message = $('[class*="alert-wrap"]');
    submitbutton.on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        submitContact.find("input, textarea, select").attr('data-error', 'false');
        $.ajax({
            type: "POST",
            url: "{{route('register-yokesen-webinar')}}",
            dataType: 'json',
            cache: false,
            data: $('#register-form').serialize(),
            success: function(data) {
                if (data.success) {
                    submitContact.find("[class*='field']").hide();
                    message.find('p').text(data.msg);
                    message.attr('data-active', 'true').addClass('success').delay(5000).fadeOut('slow').removeClass('success');
                } else {
                    var errors = data.errors;
                    errors.map(function(data) {
                        console.log('data.id', data.id);
                        submitContact.find(data.id).attr('data-error', 'true');
                    })
                    message.find('p').text(errors[0].msg);
                    message.attr('data-active', 'true');
                    if (errors.length > 0) {
                        $(`${errors[0].id}`).focus();
                    }
                }
            }
        });
    });
</script>
@endsection