@extends('templates.master')
@php
$noheaderimage=true;
@endphp

@section('og-image',$event->event_image_1)
@section('og-title',$event->blogTitle)
@section('og-description',$event->blogMetaDescription)

@section('cssonpage')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding="top">
    <div class="container">
        <!-- <div class="row" data-padding="smbottom">
            <div class="col-12">
                <div class="sectiontitle-element--center">
                    <h1 data-aos="fade-up" data-aos-delay="100" class="big">Terimakasih!</h1>
                    <p data-aos="fade-up" data-aos-delay="200">Data yang sudah disubmit akan kami proses, nanti akan ada dari tim kami yang menghubungi anda.</p>
                    <time data-aos="fade-up" data-aos-delay="400" class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($event->created_at))}}</time>
                </div>
            </div>
        </div> -->
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content" data-padding="top">
    <!--  Main Img  -->
    <!-- <section data-padding="bottom">
        <div class="container">
            <div data-aos="zoom-in" data-aos-duration="1000" class="row">
                <div class="col-12">
                    <div class="wrapimage-element">
                        <img data-unveil src="#" data-src="{{url('/').'/'.$event->event_image_1}}" data-src-retina="{{url('/').'/'.$event->event_image_1}}" alt="{{$event->blogMetaKeywords}}">
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--  END Main Img  -->
    <!--  Text  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade-up" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="sectiontitle-element--center">
                        <h1 data-aos="fade-up" data-aos-delay="100" class="big">Terima kasih!</h1>
                        <p data-aos="fade-up" data-aos-delay="200">Data yang sudah disubmit akan kami proses, selanjutnya akan ada dari tim kami yang menghubungi anda.</p>
                        <!-- <time data-aos="fade-up" data-aos-delay="400" class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($event->created_at))}}</time> -->
                    </div>

                    <div class="sectiontitle-element--center" data-padding="bottom">
                        <ul>
                            <li data-margin="col"><a href="https://www.facebook.com/Yokesen-Technology-116526403230902" target="_blank" class="simple"><i class="fab fa-facebook-f"></i> Yokesen Technology</a></li>
                            <li data-margin="col"><a href="https://www.linkedin.com/company/zeroconsultant-com" target="_blank" class="simple"><i class="fab fa-linkedin-in"></i> YOKESEN.COM</a></li>
                            <li data-margin="col"><a href="https://www.instagram.com/yokesen_id/" target="_blank" class="simple"><i class="fab fa-instagram"></i> @yokesen_id</a></li>
                            <li data-margin="col"><a href="{{env('APP_URL')}}" class="simple"><i class="fa fa-globe"></i> www.yokesen.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--  END Page Content  -->
@endsection

@section('jsonpage')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('[name="have_online_shop"]').on('click', function() {
            var radioValue = $("[name='have_online_shop']:checked").val();
            if (radioValue == 1) {
                $('.field-online-shop').show();
            } else {
                $('.field-online-shop').hide();
            }
        });
    });


    // var submitform = $('#commentform'),
    //     message = $('[class*="alert-wrap"]');

    // submitform.on('submit', function(e) {
    //     e.preventDefault();
    //     var $this = $(this);
    //     submitform.find("input, textarea, select").attr('data-error', 'false');
    //     $.ajax({
    //         type: "POST",
    //         url: "{{route('postcommentblog')}}",
    //         dataType: 'json',
    //         cache: false,
    //         data: submitform.serialize(),
    //         success: function(data) {
    //             if (data.success) {
    //                 submitform.find("[class*='field']").hide();
    //                 message.find('p').text(data.msg);
    //                 message.attr('data-active', 'true').addClass('success').delay(5000).fadeOut('slow').removeClass('success');
    //             } else {
    //                 var errors = data.errors;
    //                 errors.map(function(data) {
    //                     submitform.find(data.id).attr('data-error', 'true');
    //                 })
    //                 message.find('p').text(errors[0].msg);
    //                 message.attr('data-active', 'true');
    //             }
    //         }
    //     });
    // });
</script>


@endsection