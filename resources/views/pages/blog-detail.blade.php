@extends('templates.master')
@php
$noheaderimage=true;
@endphp

@section('og-image',$blog->blogImage1)
@section('og-title',$blog->blogTitle)
@section('og-description',$blog->blogMetaDescription)

@section('cssonpage')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding="top">
    <div class="container">
        <div class="row" data-padding="smbottom">
            <div class="col-12">
                <div class="sectiontitle-element--center">
                    <!-- <span data-aos="fade-up" class="toptitle">Branding</span> -->
                    <h1 data-aos="fade-up" data-aos-delay="100" class="big">{{$blog->blogTitle}}</h1>
                    <!-- <p data-aos="fade-up" data-aos-delay="200">Jessica Simpson</p> -->
                    <time data-aos="fade-up" data-aos-delay="400" class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($blog->created_at))}}</time>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Main Img  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="zoom-in" data-aos-duration="1000" class="row">
                <div class="col-12">
                    <div class="wrapimage-element">
                        <img class="lazyload" data-unveil src="#" data-src="{{url('/').'/'.$blog->blogImage1}}" data-src-retina="{{url('/').'/'.$blog->blogImage1}}" alt="{{$blog->blogMetaKeywords}}">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Main Img  -->
    <!--  Text  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade-up" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="sectiontitle-element--full">
                        <!-- <h2>{{$blog->blogTitle}}</h2> -->
                        <p>{!!$blog->blogContent!!}</p>
                    </div>
                    <!-- <h3 class="margin" data-padding="smtop">This is a subheading</h3>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor.</p>
                    <div class="list-element--plus">
                        <ul>
                            <li>Fully Resposnive</li>
                            <li>Multiple Pages</li>
                            <li>Modular Component</li>
                            <li>Create your website</li>
                        </ul>
                    </div>
                    <div data-padding="xstop"></div>
                    <blockquote class="blockquote-element--primary" data-margin>
                        It is better to lead from behind and to put others in front,especially when you celebrate victory when nice things occur. You take the front line when there is danger. Then people will appreciate your leadership.
                        <span class="author">John Doe <span>Marketing Specialist</span></span>
                    </blockquote>
                    <p data-padding="xstop">Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta crowdfunding rockstar marketing metrics supply chain network effects iPad niche market venture.</p>
                    <div data-padding="xstop"></div>
                    <div data-aos="zoom-in" class="rectangle-element--overlay--center">
                        <div class="info">
                            <div class="image">
                                <img data-unveil src="#" data-src="{{url('/')}}/img/post-4.jpg" data-src-retina="{{url('/')}}/img/post-4@2x.jpg" alt="">
                            </div>
                            <div class="text">
                                <p>Call this caption</p>
                            </div>
                        </div>
                    </div>
                    <div data-padding="xstop"></div>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta </p>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta crowdfunding rockstar marketing metrics supply chain network effects iPad niche market venture.</p>
                    <pre class="code-element">
                                    <code>&lt;p&gt;Sample text here...&lt;/p&gt;
                                    <br>&lt;p&gt;And another line of sample text here...&lt;/p&gt;
                                    </code>
                                </pre>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta</p>
                    <div data-padding="xstop"></div>
                    <h4 class="margin">This is a subheading</h4>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta crowdfunding rockstar marketing metrics supply chain network effects iPad niche market venture. Release monetization termsheet hypotheses facebook user
                        experience technology. Market product management hackathon. Validation launch party business plan user experience handshake stealth venture analytics direct mailing alpha facebook focus bandwidth marketing. Scrum project
                        value proposition venture monetization crowdsource. Scrum project monetization buyer hackathon deployment equity freemium value proposition research & development business-to-business infrastructure marketing. Facebook
                        niche market bootstrapping non-disclosure agreement focus direct mailing funding disruptive mass market.</p>
                    <div class="list-element--number">
                        <ul>
                            <li>Fully Resposnive</li>
                            <li>Multiple Pages</li>
                            <li>Modular Component</li>
                            <li>Create your website</li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!--  END Text  -->
    @if(isset($blog->blogRedirect))
    <section data-aos="fade" data-padding="xstop">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ctabox-element--center--round--light">
                        <div class="text">
                            {!!$blog->blogTextCallToAction!!}
                            <a href="{{$blog->blogRedirect}}" class="btn--big--border--light--round" target="_self">{{$blog->blogCallToAction}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif

    <!--  Share Section  -->
    <section data-padding="xstop">
        <div class="container">
            <div data-aos="fade" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="social-element--round">
                        <div class="title">
                            Share this article
                        </div>
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_inline_share_toolbox"></div>
                        <!-- <ul>
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{url()->full()}}"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->full()}}&title=&summary=&source="><i class="fab fa-linkedin-in"></i></a></li>
                            <li><a href="https://twitter.com/home?status={{url()->full()}}"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="mailto:?&subject=&body={{url()->full()}}"><i class="far fa-envelope"></i></a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Share Section  -->
    <!--  Tags Section  -->
    <section data-padding="smbottom">
        <div class="container">
            <div data-aos="fade" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="tags-element--separator">
                        <div class="title">
                            Tags:
                        </div>
                        <ul>
                            <?php $keywords = explode(" ", $blog->blogMetaKeywords); ?>
                            @foreach ($keywords as $nes)
                            <li><a href="#.">{{$nes}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- <div data-aos="fade" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="icon-element--round--left--author--wrapper">
                        <div class="info">
                            <div class="image">
                                <img data-unveil src="#" data-src="{{url('/')}}/img/author-1.jpg" data-src-retina="{{url('/')}}/img/author-1.jpg@2x.jpg" alt="">
                            </div>
                            <div class="text">
                                <h6>Jessica Simpson</h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!--  END Tags Section  -->
    <!--  Comments  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade" class="row" data-padding="xsbottom">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="sectiontitle-elemement">
                        <h2>Comments</h2>
                    </div>
                </div>
            </div>
            <div data-aos="fade" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div id="disqus_thread"></div>

                </div>
            </div>
        </div>
    </section>
    <div id="disqus_thread"></div>

    <!--  END Comments  -->
    <!--  Form  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade" class="row" data-padding="xsbottom">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="sectiontitle-elemement">
                        <h2>Leave a reply</h2>
                    </div>
                </div>
            </div>
            <div data-aos="fade" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <form id="commentform" onsubmit="event.preventDefault()">
                        @csrf
                        <input type="hidden" name="blog_id" value="{{$blog->id}}" />
                        <div class="basic-form--border--round--row">
                            <div class="field col-lg-6">
                                <input id="name" name="name" type="text" placeholder="Name" class="form-field">
                            </div>
                            <div class="field col-lg-6">
                                <!-- data-error="true" to set error field -->
                                <input id="email" name="email" type="email" placeholder="Email" class="form-field" data-error="false">
                            </div>
                            <div class="field col-12">
                                <!-- data-error="true" to set error field -->
                                <textarea id="message" name="message" placeholder="Reply" class="form-field"></textarea>
                            </div>
                            <!-- data-active="false" is hidden, data-active="true" is visible -->
                            <div class="alert-wrap col-12" data-active="false">
                                <p>Error Warning</p>
                            </div>
                            <div class="field col-12">
                                <input id="submit" type="submit" class="btn--big--round" value="Add Reply">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--  END Form  -->
    <!--  Articles  -->
    @if(count($moreblogs))
    <section data-padding data-bg="grey">
        <div class="container">
            <div data-aos="fade" class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--center">
                        <h2>More Articles</h2>
                    </div>
                </div>
            </div>
            <div class="row" data-padding="xstop">
                @foreach($moreblogs as $key=>$blog)
                <div data-aos="fade" data-aos-delay="{{$key+1}}00" class="col-12 col-md-4">
                    <!--  Single Article  -->
                    <article class="rectangle-element--article--wide">
                        <div class="info">
                            <div class="image">
                                <img data-unveil src="#" data-src="{{url('/').'/'.$blog->blogImage1}}" data-src-retina="{{url('/').'/'.$blog->blogImage1}}" alt="">
                            </div>
                            <div class="text">
                                <div class="meta--image">
                                    <div class="author--round" style="background-color: black;">
                                        <img data-unveil src="#" data-src="{{url('/')}}/images/yokesen.png" data-src-retina="{{url('/')}}/images/yokesen.png" alt="" style="height: 78% !important;">
                                    </div>
                                    <div class="tags">
                                        <time class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($blog->created_at))}}</time>
                                        <ul class="categories">
                                            <ul class="categories">
                                                <?php $keywords = explode(" ", $blogpopular->blogMetaKeywords); ?>
                                                @foreach ($keywords as $nes)
                                                <li><a href="#.">{{$nes}}</a></li>
                                                @endforeach
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                                <h3>{{$blog->blogTitle}}</h3>
                                <p>{{str_limit($blog->blogMetaDescription,50)}}</p>
                                <a href="{{route('blog-detail',['slug'=>$blog->blogSlug])}}" class="simple--underline">{{trans('page.read_all')}}</a>
                            </div>
                        </div>
                        <a href="{{route('blog-detail',['slug'=>$blog->blogSlug])}}" class="link"></a>
                    </article>
                    <!--  END: Single Article  -->
                </div>

                @endforeach

            </div>
        </div>
    </section>
    @endif
    <!--  END Articles  -->
</div>
<!--  END Page Content  -->
@endsection

@section('jsonpage')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var submitform = $('#commentform'),
        message = $('[class*="alert-wrap"]');

    submitform.on('submit', function(e) {
        e.preventDefault();
        var $this = $(this);
        submitform.find("input, textarea, select").attr('data-error', 'false');
        $.ajax({
            type: "POST",
            url: "{{route('postcommentblog')}}",
            dataType: 'json',
            cache: false,
            data: submitform.serialize(),
            success: function(data) {
                if (data.success) {
                    submitform.find("[class*='field']").hide();
                    message.find('p').text(data.msg);
                    message.attr('data-active', 'true').addClass('success').delay(5000).fadeOut('slow').removeClass('success');
                } else {
                    var errors = data.errors;
                    errors.map(function(data) {
                        submitform.find(data.id).attr('data-error', 'true');
                    })
                    message.find('p').text(errors[0].msg);
                    message.attr('data-active', 'true');
                }
            }
        });
    });
</script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5eb0fd8bf97263d3"></script>

<script>
    /**
     *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
     *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
        var d = document,
            s = d.createElement('script');
        s.src = 'https://dev-yokesen.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@endsection