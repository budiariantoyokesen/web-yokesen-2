@extends('templates.master')
@php
$noheaderimage=true;
@endphp

@section('og-image',$event->event_image_1)
@section('og-title',$event->blogTitle)
@section('og-description',$event->blogMetaDescription)

@section('cssonpage')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding="top">
    <div class="container">
        <div class="row" data-padding="smbottom">
            <div class="col-12">
                <div class="sectiontitle-element--center">
                    <!-- <span data-aos="fade-up" class="toptitle">Branding</span>
                    <h1 data-aos="fade-up" data-aos-delay="100" class="big">{{$event->event_title}}</h1>
                    <p data-aos="fade-up" data-aos-delay="200">Jessica Simpson</p>
                    <time data-aos="fade-up" data-aos-delay="400" class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($event->created_at))}}</time> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Main Img  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="zoom-in" data-aos-duration="1000" class="row">
                <div class="col-12">
                    <div class="wrapimage-element" style="padding: 44px;">
                        <img data-unveil src="#" data-src="{{url('/').'/'.$event->event_image_1}}" data-src-retina="{{url('/').'/'.$event->event_image_1}}" alt="{{$event->blogMetaKeywords}}">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Main Img  -->
    @if($event->status == 'Active')
    <!--  Text  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade-up" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="sectiontitle-element--full">
                        <p>{!!$event->event_content!!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Text  -->

    <!--  Form  -->
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade" class="row">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <form id="Submitform" method="post" action="{{route('event-register')}}">
                        @csrf
                        <input type="hidden" name="event_title_slug" value="{{$event->event_title_slug}}" />
                        <div class="basic-form--border--round--row">
                            <div class="field col-lg-12">
                                <input id="name" name="name" type="text" placeholder="Nama lengkap" class="form-field" data-error="{{$errors->error->has('name')==1?'true':'false'}}" value="{{old('name')}}" required>
                            </div>
                            <div class="field col-12">
                                <!-- data-error="true" to set error field -->
                                <input id="phone" name="phone" type="text" placeholder="Masukkan No Hp" class="form-field" data-error="{{$errors->error->has('phone')==1?'true':'false'}}" value="{{old('phone')}}" required>
                                <span class="info text-left" data-active="true">Masukkan No Hp yang dapat dihubungi melalui whatsapp</span>
                            </div>
                            <div class="field col-12">
                                <!-- data-error="true" to set error field -->
                                <input id="email" name="email" type="email" placeholder="Email" class="form-field" data-error="{{$errors->error->has('email')==1?'true':'false'}}" value="{{old('email')}}" required>
                            </div>
                            <div class="field col-12" id="privacy-wrap">
                                <input class="styled-checkbox" id="privacy-form" type="radio" name="have_online_shop" value="1" {{old('have_online_shop')=="1"?'checked':''}} required>
                                <label for="privacy-form">saya sudah memiliki toko online</label>
                                <input class="styled-checkbox" id="privacy-form" type="radio" name="have_online_shop" value="0" {{old('have_online_shop')=="0"?'checked':''}} required>
                                <label for="privacy-form">saya belum memiliki toko online</label>
                            </div>
                            <div class="field col-12 hidden field-online-shop">
                                <h5>Nama Toko Online</h5>
                                <div class="field col-12">
                                    <!-- data-error="true" to set error field -->
                                    <input name="online_shop_name" type="text" placeholder="Nama toko online" class="form-field" data-error="{{$errors->error->has('online_shop_name')==1?'true':'false'}}" value="{{old('online_shop_name')}}">
                                </div>
                                <div class="field col-12" id="privacy-wrap">
                                    <input class="styled-checkbox" id="privacy-form" type="checkbox" name="ecoomerce[]" value="tokopedia" {{!empty(old('ecoomerce'))&&in_array('tokopedia',old('ecoomerce'))?'checked':''}}>
                                    <label for="privacy-form">Tokopedia</label>
                                    <input class="styled-checkbox" id="privacy-form" type="checkbox" name="ecoomerce[]" value="shopee" {{!empty(old('ecoomerce'))&&in_array('shopee',old('ecoomerce'))?'checked':''}}>
                                    <label for="privacy-form">Shopee</label>
                                    <input class="styled-checkbox" id="privacy-form" type="checkbox" name="ecoomerce[]" value="blibli" {{!empty(old('ecoomerce'))&&in_array('blibli',old('ecoomerce'))?'checked':''}}>
                                    <label for="privacy-form">Blibli</label>
                                    <input class="styled-checkbox" id="privacy-form" type="checkbox" name="ecoomerce[]" value="bukalapak" {{!empty(old('ecoomerce'))&&in_array('bukalapak',old('ecoomerce'))?'checked':''}}>
                                    <label for="privacy-form">Bukalapak</label>
                                    <input class="styled-checkbox" id="privacy-form" type="checkbox" name="ecoomerce[]" value="jd-id" {{!empty(old('ecoomerce'))&&in_array('jd-id',old('ecoomerce'))?'checked':''}}>
                                    <label for="privacy-form">JD.ID</label>
                                    <div class="alert-wrap" data-active="{{$errors->error->has('ecoomerce')==1?'true':'false'}}">
                                        <p>Pilih salah satu</p>
                                    </div>
                                </div>
                                <div class="field col-12">
                                    <!-- data-error="true" to set error field -->
                                    <input name="online_shop_others" type="text" placeholder="Others" class="form-field" data-error="false">
                                </div>

                            </div>

                            <!-- data-active="false" is hidden, data-active="true" is visible -->
                            <div class="alert-wrap col-12" data-active="false">
                                <p>Error Warning</p>
                            </div>
                            <div class="field col-12">
                                <input id="submit" type="submit" class="btn--big--round" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--  END Form  -->

    @endif

    @if($event->status == 'Expired')
    <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade-up" class="row aos-init aos-animate">
                <div class="col-12 col-lg-8 offset-lg-2">
                    <div class="sectiontitle-element--full">
                        <p class="text-center">Event webinar ini sudah berakhir tunggu webinar kami yang lain .</p>
                        @if(!empty($nextevent))
                        <br>
                        <div class="field col-12 text-center">
                            <a class="btn--big--round" style="color:white;cursor:pointer;" href="{{route('webinar-page', ['event_title_slug'=>$nextevent->event_title_slug])}}">Cari tau</a>
                        </div>
                        @endif
                    </div>
                    <!-- <h3 class="margin" data-padding="smtop">This is a subheading</h3>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor.</p>
                    <div class="list-element--plus">
                        <ul>
                            <li>Fully Resposnive</li>
                            <li>Multiple Pages</li>
                            <li>Modular Component</li>
                            <li>Create your website</li>
                        </ul>
                    </div>
                    <div data-padding="xstop"></div>
                    <blockquote class="blockquote-element--primary" data-margin>
                        It is better to lead from behind and to put others in front,especially when you celebrate victory when nice things occur. You take the front line when there is danger. Then people will appreciate your leadership.
                        <span class="author">John Doe <span>Marketing Specialist</span></span>
                    </blockquote>
                    <p data-padding="xstop">Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta crowdfunding rockstar marketing metrics supply chain network effects iPad niche market venture.</p>
                    <div data-padding="xstop"></div>
                    <div data-aos="zoom-in" class="rectangle-element--overlay--center">
                        <div class="info">
                            <div class="image">
                                <img data-unveil src="#" data-src="https://dev-2.yokesen.com/img/post-4.jpg" data-src-retina="https://dev-2.yokesen.com/img/post-4@2x.jpg" alt="">
                            </div>
                            <div class="text">
                                <p>Call this caption</p>
                            </div>
                        </div>
                    </div>
                    <div data-padding="xstop"></div>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta </p>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta crowdfunding rockstar marketing metrics supply chain network effects iPad niche market venture.</p>
                    <pre class="code-element">
                                    <code>&lt;p&gt;Sample text here...&lt;/p&gt;
                                    <br>&lt;p&gt;And another line of sample text here...&lt;/p&gt;
                                    </code>
                                </pre>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta</p>
                    <div data-padding="xstop"></div>
                    <h4 class="margin">This is a subheading</h4>
                    <p>Hackathon handshake pivot crowdsource first mover advantage partnership. Prototype accelerator infographic backing ownership strategy focus user experience branding. Infographic metrics angel investor founders series A financing
                        value proposition buyer growth hacking. Paradigm shift android beta crowdfunding rockstar marketing metrics supply chain network effects iPad niche market venture. Release monetization termsheet hypotheses facebook user
                        experience technology. Market product management hackathon. Validation launch party business plan user experience handshake stealth venture analytics direct mailing alpha facebook focus bandwidth marketing. Scrum project
                        value proposition venture monetization crowdsource. Scrum project monetization buyer hackathon deployment equity freemium value proposition research & development business-to-business infrastructure marketing. Facebook
                        niche market bootstrapping non-disclosure agreement focus direct mailing funding disruptive mass market.</p>
                    <div class="list-element--number">
                        <ul>
                            <li>Fully Resposnive</li>
                            <li>Multiple Pages</li>
                            <li>Modular Component</li>
                            <li>Create your website</li>
                        </ul>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    @endif

</div>
<!--  END Page Content  -->
@endsection

@section('jsonpage')
<script>
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var haveonlineshop = "{{old('have_online_shop')}}";
        if (haveonlineshop == "1") {
            $('.field-online-shop').show();
        }
        console.log('haveonlineshop', haveonlineshop);
        $('[name="have_online_shop"]').on('click', function() {
            var radioValue = $("[name='have_online_shop']:checked").val();
            if (radioValue == 1) {
                $('.field-online-shop').show();
            } else {
                $('.field-online-shop').hide();
            }
        });
    });


    // var submitform = $('#commentform'),
    //     message = $('[class*="alert-wrap"]');

    // submitform.on('submit', function(e) {
    //     e.preventDefault();
    //     var $this = $(this);
    //     submitform.find("input, textarea, select").attr('data-error', 'false');
    //     $.ajax({
    //         type: "POST",
    //         url: "{{route('postcommentblog')}}",
    //         dataType: 'json',
    //         cache: false,
    //         data: submitform.serialize(),
    //         success: function(data) {
    //             if (data.success) {
    //                 submitform.find("[class*='field']").hide();
    //                 message.find('p').text(data.msg);
    //                 message.attr('data-active', 'true').addClass('success').delay(5000).fadeOut('slow').removeClass('success');
    //             } else {
    //                 var errors = data.errors;
    //                 errors.map(function(data) {
    //                     submitform.find(data.id).attr('data-error', 'true');
    //                 })
    //                 message.find('p').text(errors[0].msg);
    //                 message.attr('data-active', 'true');
    //             }
    //         }
    //     });
    // });
</script>


@endsection