@extends('templates.master')
@section('cssonpage')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@php
$noheaderimage=true;
@endphp
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding="top">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="sectiontitle-element">
                    <h1 data-aos="fade" class="big">{{trans('page.contact-header-text')}}</h1>
                    <!-- <p data-aos="fade" data-aos-delay="200">User experience business-to-business ramen pitch infrastructure seed money analytics channels burn rate</p> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Icons Section  -->
    <section data-padding="sm" data-bg="grey">
        <div class="container">
            <div class="row">
                <div data-aos="fade-up" data-aos-delay="100" class="col-12 col-sm-4">
                    <div class="icon-element--square--left">
                        <div class="info">
                            <div class="icon--colortwo"><i class="feather icon-edit-2"></i></div>
                            <div class="text">
                                <h6>{{trans('page.email-us')}}</h6>
                                <p><a href="mailto:contact@yokesen.com" class="simple-underline">contact@yokesen.com</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" data-aos-delay="200" class="col-12 col-sm-4">
                    <div class="icon-element--square--left">
                        <div class="info">
                            <div class="icon--colortwo"><i class="feather icon-layout"></i></div>
                            <div class="text">
                                <h6>{{trans('page.visit-us')}}</h6>
                                <p>Ruko Crystal 8 No 18. Pakualam, Kec. Serpong Utara, Kota Tangerang Selatan, Banten 15320</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" data-aos-delay="300" class="col-12 col-sm-4">
                    <div class="icon-element--square--left">
                        <div class="info">
                            <div class="icon--colortwo"><i class="feather icon-code"></i></div>
                            <div class="text">
                                <h6>{{trans('page.call-us')}}</h6>
                                <p><a href="tel:(021) 22301202" class="simple-underline">(021) 22301202</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Icons Section  -->
    <section data-padding>
        <div class="container">
            <div class="row align-items-lg-center">
                <div data-aos="fade" class="map-element col-12 col-lg-6 col-xl-5">
                    <div id="map"></div>
                    <!-- <div class="sectiontitle-element">
                        <h2>Get in touch</h2>
                        <p>Holy grail bandwidth stealth niche market freemium buyer traction. A/B testing paradigm shift stealth return on investment android startup user experience bootstrapping funding partnership agile development innovator network
                            effects. Beta series A financing buzz creative. </p>
                        <div class="list-element--plus">
                            <ul>
                                <li>Fully Responsive</li>
                                <li>Multiple Page</li>
                                <li>Modular Component</li>
                                <li>Create your website</li>
                            </ul>
                        </div>
                    </div> -->
                </div>
                <!--  Form. For the settings contact.php  -->
                <div data-aos="fade" data-aos-delay="200" class="col-12 col-lg-6 col-xl-6 offset-xl-1">
                    <form id="contact-form">
                        <div class="basic-form--border--round--row">
                            <div class="field col-lg-12">
                                <label for="name">{{trans('page.your-name')}} <sup>*</sup></label>
                                <input id="name" name="name" type="text" class="form-field" placeholder="{{trans('page.your-name')}}">
                            </div>
                            <div class="field col-lg-12">
                                <label for="mail">{{trans('page.email-address')}} <sup>*</sup></label>
                                <!-- data-error="true" to set error field -->
                                <input id="mail" name="mail" type="email" placeholder="{{trans('page.email-address')}}" class="form-field" data-error="false">
                            </div>
                            <div class="field col-12">
                                <label for="message">{{trans('page.message')}} <sup>*</sup></label>
                                <!-- data-error="true" to set error field -->
                                <textarea id="message" name="message" class="form-field" placeholder="{{trans('page.message')}}"></textarea>
                            </div>
                            <!-- data-active="false" is hidden, data-active="true" is visible -->
                            <!-- <div class="alert-wrap col-12" data-active="false">
                                <p>For the error message see contact.php file</p>
                            </div> -->
                            <div class="field col-12">
                                <input id="submit" type="submit" class="btn--big--round" value="{{trans('page.send-message')}}">
                            </div>
                        </div>
                    </form>
                </div>
                <!--  END Form. For the settings contact.php  -->
            </div>
        </div>
    </section>
</div>
<!--  END Page Content  -->
@endsection

@section('gmaps')<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false"></script> @endsection
@section('jsonpage')
<script src="{{url('/')}}/js/maps.js"></script>
<script src="{{url('/')}}/js/jquery.validate/jquery.validate.min.js"></script>
<!-- <script src="{{url('/')}}/js/vendor.js"></script> -->

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var submitContact = $('#contact-form'),
        message = $('[class*="alert-wrap"]');
    submitContact.on('click', function(e) {
        e.preventDefault();
        var $this = $(this);
        submitContact.find("input, textarea, select").attr('data-error', 'false');
        $.ajax({
            type: "POST",
            url: "{{route('send-message')}}",
            dataType: 'json',
            cache: false,
            data: $('#contact-form').serialize(),
            success: function(data) {

                console.log(data);
                if (data.success) {
                    submitContact.find("[class*='field']").hide();
                    message.find('p').text(data.msg);
                    message.attr('data-active', 'true').addClass('success').delay(5000).fadeOut('slow').removeClass('success');
                } else {
                    var errors = data.errors;
                    errors.map(function(data) {
                        submitContact.find(data.id).attr('data-error', 'true');
                    })
                    message.find('p').text(errors[0].msg);
                    message.attr('data-active', 'true');

                }
            }
        });
    });
</script>
@endsection