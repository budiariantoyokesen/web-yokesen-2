@extends('templates.master')
@php
$noheaderimage=true;
@endphp
@section('content')

<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding>
    <div class="container">
        <div class="row align-items-lg-center">
            <div class="col-12 col-lg-6 col-xl-5">
                <div class="sectiontitle-element">
                    <span data-aos="fade-up" class="toptitle--bg--round--colortwo">{{$portfolio->tags_service}}</span>
                    <h3 data-aos="fade-up" data-aos-delay="100" class="big">{{$portfolio->name}}</h3>
                    {!!$portfolio->description!!}
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-6 offset-xl-1">
                <div class="wrapimage-element--square">
                    <img data-unveil data-src="{{url('/').'/'.$portfolio->image_url}}" data-src-retina="{{url('/').'/'.$portfolio->image_url}}" alt="{{!empty($portfolio->alt_text)?$portfolio->alt_text:$portfolio->name.' - Yokesen - '.$portfolio->tags_service}}">
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    @if(!empty($portfolio->section_2_image_url))
    <section data-padding data-bg="grey">
        <div class="container">
            <div data-aos="fade-up" class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--center">
                        <h2>{{$portfolio->section_2_title}}</h2>
                    </div>
                </div>
            </div>
            <div class="row align-items-lg-center" data-padding="xstop">
                <div data-aos="zoom-in" class="col-12 col-lg-6">
                    <div class="wrapimage-element--square">
                        <div class="image">
                            <img class="img-responsive" data-unveil data-src="{{url('/').'/'.$portfolio->section_2_image_url}}" data-src-retina="{{url('/').'/'.$portfolio->section_2_image_url}}" alt="{{!empty($portfolio->alt_text)?$portfolio->alt_text:$portfolio->name.' - Yokesen - '.$portfolio->tags_service}}">
                        </div>
                    </div>
                </div>
                <div data-aos="fade" data-aos-delay="200" class="col-12 col-lg-6">
                    <div class="icon-element" data-padding>
                        <div class="info">
                            {!!$portfolio->section_2_right!!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    @endif
    <section data-aos="fade" data-padding>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--inline align-items-md-end">
                        <div class="wrapper">
                            <span class="toptitle">Related Project</span>
                            <h2>Related Project</h2>
                        </div>
                        <!--  Portfolio Nav  -->
                        <div class="wrapper portfolio-wrap">
                            <div class="navigation-element--border--round--light">
                                <div class="navigation justify-content-md-end">
                                    <div class="nav--prev">
                                        <i class="feather icon-arrow-left"></i>
                                    </div>
                                    <div class="nav--next">
                                        <i class="feather icon-arrow-right"></i>
                                    </div>
                                    <a href="{{route('portfolios')}}" class="btn--big--round">{{trans('page.view-all')}}</a>
                                </div>
                            </div>
                        </div>
                        <!--  END: Portfolio Nav  -->
                    </div>
                </div>
            </div>
            <div class="row" data-padding="xstop">
                <div class="col-12">
                    <div class="portfolio-carousel owl-carousel">
                        @foreach($related as $data )
                        <div class="square-element--project">
                            <div class="info">
                                <div class="image">
                                    <img class="owl-lazy" data-src="{{url('/').'/'.$data->image_url}}" data-src-retina="{{url('/').'/'.$data->image_url}}" alt="{{!empty($data->alt_text)?$data->alt_text:$data->name.' - Yokesen - '.$data->tags_service}}">
                                    <!-- <div class="icon--br--square--secondary"><i class="feather icon-plus"></i></div> -->
                                </div>
                                <div class="text">
                                    <h6>{{$data->name}}</h6>
                                    <p>{{$data->tags_service}}</p>
                                </div>
                            </div>
                            <a href="{{route('detail-portfolio',['portfolio_slug'=>$data->portfolio_slug])}}" class="link"></a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--  END Page Content  -->
@endsection