@extends('templates.master')
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header--image" data-overlay>
    <div class="imagebg" style="background-image: url({{url('/')}}/img/about-header@2x.jpg);"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-7">
                <div class="sectiontitle-element--light">
                    <h1>Happiness is just one word</h1>
                    <h1>with a "share" </h1>
                    <h1>button in it</h1>
                    <p></p>
                    <br>
                </div>
                <!--  Breadcrumbs  -->
                <div class="list-element--text--breadcrumbs--arrow--light">
                    <ul>
                        <li><a href="{{route('homeagency')}}">Home</a></li>
                        <li>About Yokesen CSR</li>
                    </ul>
                </div>
                <!--  END Breadcrumbs  -->
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Intro  -->
    <div class="page-header" data-padding="">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--center">
                        <span data-aos="fade-up" class="toptitle aos-init aos-animate">Yokesen</span>
                        <h3 data-aos="fade-up" data-aos-delay="100" class="big aos-init aos-animate">Corporate Sosial Responsibility</h3>
                        <!-- <p data-aos="fade-up" data-aos-delay="200" class="aos-init aos-animate">User experience business-to-business ramen pitch infrastructure seed money analytics channels burn rate</p> -->
                    </div>
                </div>
                <div class="col-12">
                    <div class="sectiontitle-element justify-content-md-between">
                        <p>CSR (Corporate Social Responsibility) is a form of company's ongoing commitment to the environment and society through various activities in improving the environment and community welfare. Not only looking for the company's financial benefits, yet to balance the environment, society and company’s sustainabilty.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  END Intro  -->
    <!-- <section data-padding="bottom">
        <div class="container">
            <div data-aos="zoom-in" data-aos-duration="1000" class="row aos-init aos-animate">
                <div class="col-12">
                    <div class="wrapimage-element">
                        <img data-unveil="" src="{{url('/')}}/img/project-header.jpg" data-src="{{url('/')}}/img/project-header.jpg" data-src-retina="{{url('/')}}/img/project-header@2x.jpg" alt="" style="opacity: 1;">
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!-- <section data-padding="bottom">
        <div class="container">
            <div data-aos="fade-up" class="row aos-init aos-animate">
                <div class="col-12">
                    <div class="sectiontitle-element">
                        <h2>What we did</h2>
                        <p>Holy grail bandwidth stealth niche market freemium buyer traction. A/B testing paradigm shift stealth return on investment android startup user experience bootstrapping funding partnership agile development innovator network
                            effects. Beta series A financing buzz creative.</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <!--  Full Width Row  -->
    <section class="fullrow-element--image" data-margin="xstop" data-bg="template">
        <div class="image" style="background-image: url({{url('/')}}/img/team-corporate@2x.jpg);"></div>
        <div class="main-wrapper">
            <div class="inner-wrapper">
                <div class="sectiontitle-element">
                    <h4>Yok Bantu</h4>
                    <h2>Yokesen Membantu</h2>
                    <p>As a company engaged in the field of digital and technology, Yokesen understands that digital and technology are constantly evolving over time. We also understand that humans cannot be separated from technology and digital in order to meet the needs of daily life, which are receiving information, looking for entertainment, doing shopping transactions, and so on.</p>
                    <p>In commemoration of Yokesen's 1st Anniversary, we began to play an active role in community service. We call it "Yok Bantu" or Yokesen Membantu. In accordance with the industry we're engaged in, Yokesen aims to help people manage their business by utilizing digital and technology.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- <section data-padding="">
        <div class="container">
            <div data-aos="fade-up" class="row align-items-lg-center aos-init aos-animate">
                <div class="col-12 col-lg-6">
                    <div class="wrapimage-element--square">
                        <img data-unveil="" src="{{url('/')}}/img/about-gallery.jpg" data-src="{{url('/')}}/img/about-gallery.jpg" data-src-retina="assets/img/about-gallery@2x.jpg" alt="" style="opacity: 1;">
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-5 offset-xl-1">
                    <div class="sectiontitle-element">
                        <h4>Yok Bantu</h4>
                        <h2>Yokesen Membantu</h2>
                        <div class="separator-element"></div>
                        <p>Sebagai perusahan yang bergerak di bidang digital dan teknologi, Yokesen memahami bahwa digital dan teknologi selalu berkembang setiap harinya tiada henti. Yokesen juga memahami bahwa kini manusia tidak bisa lepas dari teknologi dan digital dalam memenuhi kebutuhan hidup sehari-hari, baik menerima informasi, mencari hiburan, melakukan transaksi belanja dan sebagainya. </p>
                        <p>Dalam memperingati 1 tahun Yokesen (1st Anniversary), kami mulai berperan aktif dalam aksi sosial. Aksi sosial ini kami sebut “Yok Bantu” atau Yokesen Membantu. Sesuai dengan ranah bisnis, Yokesen bertujuan untuk membantu masyarakat dalam mengelola bisnisnya dengan memanfaatkan digital dan teknologi.</p>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <section data-padding="">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sectiontitle-element">
                        <h2 data-aos="fade-up" class="aos-init aos-animate">Yok Bantu Pengembangan Bisnis</h2>
                        <p data-aos="fade-up" data-aos-delay="200" class="aos-init aos-animate">Yokesen's first social event was based on the financial/income adversity of individuals and companies in the middle of the pandemic. Therefore, we took the initiative by providing training and monitoring for people who need to maintain their business through digital, especially in Tokopedia. Starts from May 1st to June 1st 2020. It is free of charge, there is only one thing you need to do to join the webinar. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Full Width Row  -->
    <!--  CTA Box  -->
    <section data-aos="fade" data-padding="xstop">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ctabox-element--center--round--light">
                        <div class="text">
                            <h2>Are you interested?</h2>
                            <p>Tap the button below and follow further instructions</p>
                            <a href="{{route('yokesen-webinar')}}" class="btn--big--border--light--round" target="_self">Participate Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END CTA Box  -->
</div>
@endsection