@extends('templates.master')
@php
$noheaderimage=true;
@endphp
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header" data-padding>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12">
                <!--  Article Featured  -->
                <article class="rectangle-element--article--wide--inline">
                    <div class="info">
                        <div class="image">
                            <img data-unveil data-src="{{url('/').'/'.$blogpopular->blogImage1}}" data-src-retina="{{url('/').'/'.$blogpopular->blogImage1}}" alt="{{$blogpopular->BlogMetaKeywords}}">
                        </div>
                        <div class="text">
                            <div class="meta--image">
                                <div class="author--round" style="background-color: black;">
                                    <img data-unveil data-src="{{url('/')}}/images/yokesen.png" data-src-retina="{{url('/')}}/images/yokesen.png" alt="Yokesen" style="height: 78% !important;">
                                </div>
                                <div class="tags">
                                    <time class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($blogpopular->created_at))}}</time>
                                    <ul class="categories">
                                        <?php $keywords = explode(" ", $blogpopular->blogMetaKeywords); ?>
                                        @foreach ($keywords as $nes)
                                        <li><a href="#.">{{$nes}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <h3>{{$blogpopular->blogTitle}}</h3>
                            <p>{{str_limit($blogpopular->blogMetaDescription,50)}}</p>
                            <a href="{{route('blog-detail',['slug'=>$blogpopular->blogSlug])}}" class="simple--underline">{{trans('page.read_all')}}</a>
                        </div>
                    </div>
                    <a href="{{route('blog-detail',['slug'=>$blogpopular->blogSlug])}}" class="link"></a>
                </article>
                <!--  END Article Featured  -->
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <section data-padding="bottom">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--inline align-items-md-end" data-padding="none">
                        <div class="wrapper">
                            <h2>{{trans('page.new_blog_post')}}</h2>
                        </div>
                        <div class="wrapper articles-wrap">
                            <div class="navigation-element--border--round--light" data-padding="none">
                                <div class="navigation justify-content-md-end">
                                    <div class="nav--prev">
                                        <i class="feather icon-arrow-left"></i>
                                    </div>
                                    <div class="nav--next">
                                        <i class="feather icon-arrow-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" data-padding="xs">
                <div class="col-12">
                    <div class="articles-carousel owl-carousel">
                        @foreach($newestblogs as $key=>$blog)
                        <article class="rectangle-element--article--wide">
                            <div class="info">
                                <div class="image">
                                    <img data-unveil data-src="{{url('/').'/'.$blog->blogImage1}}" data-src-retina="{{url('/').'/'.$blog->blogImage1}}" alt="{{$blog->BlogMetaKeywords}}">
                                </div>
                                <div class="text">
                                    <div class="meta--image">
                                        <div class="author--round" style="background-color: black;">
                                            <img data-unveil src="#" data-src="{{url('/')}}/images/yokesen.png" data-src-retina="{{url('/')}}/images/yokesen.png" alt="" style="height: 78% !important;">
                                        </div>
                                        <div class="tags">
                                            <time class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($blog->created_at))}}</time>
                                            <ul class="categories">
                                                <?php $keywords = explode(" ", $blog->blogMetaKeywords); ?>
                                                @foreach ($keywords as $nes)
                                                <li><a href="#.">{{$nes}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <h3>{{$blog->blogTitle}}</h3>
                                    <p>{{str_limit($blog->blogMetaDescription,50)}}</p>
                                    <a href="{{route('blog-detail',['slug'=>$blog->blogSlug])}}" class="simple--underline">{{trans('page.read_all')}}</a>
                                </div>
                            </div>
                            <a href="{{route('blog-detail',['slug'=>$blog->blogSlug])}}" class="link"></a>
                        </article>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!--  END Page Content  -->
@endsection