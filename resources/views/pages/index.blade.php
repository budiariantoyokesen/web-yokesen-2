@extends('templates.master')
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header--image" data-padding data-overlay>
    <!--  you can set a background instead of an img: ex (<div class="imagebg" style="background-image: url(assets/img/header-home@2x.jpg);">)  -->
    <div class="imagebg" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant">
        <img data-unveil data-src="{{url('/')}}/images/home/Header.png" data-src-retina="{{url('/')}}/images/home/Header.png" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant">
    </div>
    <div class="container">
        <div class="row">
            <div data-aos="fade-up" class="col-12 col-lg-8 col-xl-7">
                <div class="sectiontitle-element--light">
                    <h1 class="big">{!!trans('page.home-banner-text-1')!!}</h1>
                    <p>{!!trans('page.home-banner-text-2')!!}</p>
                    <a href="{{route('about-yokesen')}}" class="btn--big--round--border--light">{{trans('page.learn-more')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<div class="page-content">
    <!--  Intro Section  -->
    <section data-padding>
        <div class="container">
            <div data-aos="fade-up" class="row" data-padding="xstop">
                <div class="col-12">
                    <div class="sectiontitle-element--center">
                        <span class="toptitle">{{trans('page.what-we-do')}}</span>
                        <h4>{{trans('page.home-section1-title')}}</h4>
                    </div>
                </div>
            </div>
            <div class="row" data-padding="xstop">
                <div data-aos="fade-up" data-aos-delay="100" class="col-12 col-md-4">
                    <div class="icon-element--round--box--border">
                        <div class="info">
                            <div class="icon"><i class="ilight feather ion-android-bulb"></i></div>
                            <div class="text">
                                <h6>{{trans('page.menu-services-digital-agency')}}</h6>
                                <p style="min-height: 185px;">{{trans('page.home-section1-digital-agency')}}</p>
                                <a href="{{route('digital-agency')}}" class="simple--arrow">{{trans('page.learn-more')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" data-aos-delay="200" class="col-12 col-md-4">
                    <div class="icon-element--round--box--border">
                        <div class="info">
                            <div class="icon"><i class="ilight feather ion-code-working"></i></div>
                            <div class="text">
                                <h6>{{trans('page.menu-services-technology')}}</h6>
                                <p style="min-height: 185px;">{{trans('page.home-section1-technology')}}</p>
                                <a href="{{route('technology')}}" class="simple--arrow">{{trans('page.learn-more')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" data-aos-delay="300" class="col-12 col-md-4">
                    <div class="icon-element--round--box--border">
                        <div class="info">
                            <div class="icon"><i class="ilight feather ion-compass"></i></div>
                            <div class="text">
                                <h6>{{trans('page.menu-services-digital-consultant')}}</h6>
                                <p style="min-height: 185px;">{{trans('page.home-section1-digital-consultant')}}</p>
                                <a href="{{route('digital-consultant')}}" class="simple--arrow">{{trans('page.learn-more')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Intro Section  -->
    <!--  Separator  -->
    <section data-aos="zoom-in" data-padding="bottom">
        <div class="container">
            <div class="row" data-padding="xs">
                <div class="col-12">
                    <div class="separator-element--lg--center"></div>
                </div>
            </div>
        </div>
    </section>
    <!--  END: Separator  -->
    <!--  Info Section  -->
    <section data-padding="bottom">
        <div class="container">
            <div class="row align-items-lg-center">
                <div data-aos="zoom-in" class="col-12 col-lg-5">
                    <div class="square-element">
                        <div class="info">
                            <div class="image">
                                <img data-unveil data-src="{{url('/')}}/images/about_us/home.png" data-src-retina="{{url('/')}}/images/about_us/home.png" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant">
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="fade-in" data-aos-delay="200" class="col-12 col-lg-6">
                    <div class="sectiontitle-element">
                        <span class="toptitle">{{trans('page.about-us')}}</span>
                        <h2>{{trans('page.aboutus-para-1')}}</h2>
                        <p>{{trans('page.aboutus-para-2')}}</p>
                        <a href="{{route('about-yokesen')}}" class="btn--big--arrow--round">{{trans('page.learn-more')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END: Info Section  -->
    <!--  List Section  -->
    <section data-padding="top" data-bg="grey">
        <div class="container">
            <div data-aos="fade-up" class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--center">
                        <span class="toptitle">Our Services</span>
                        <h2>{{trans('page.home-section-3-ourservice-title')}}</h2>
                        <!-- <a href="#" class="btn--big--arrow--round">See more</a> -->
                    </div>
                </div>
            </div>
            <div class="row" data-padding>
                <div data-aos="fade-in" data-aos-delay="100" class="col-12 col-xs-4 col-sm-4">
                    <div class="list-element--plus">
                        <p class="md f500">{{trans('page.menu-services-digital-agency')}}</p>
                        <ul>
                            <li>{{trans('page.social-media-agency')}}</li>
                            <li>{{trans('page.digital-campaign')}}</li>
                            <li>{{trans('page.digital-media-buying')}}</li>
                            <li>{{trans('page.digital-marketing-activation')}}</li>
                        </ul>
                    </div>
                    <a href="{{route('digital-agency')}}" class="btn--big--arrow--round" style="margin-top:25px;">{{trans('page.see-more')}}</a>
                </div>
                <div data-aos="fade-in" data-aos-delay="200" class="col-12 col-xs-4 col-sm-4">
                    <div class="list-element--plus">
                        <p class="md f500">{{trans('page.menu-services-technology')}}</p>
                        <ul>
                            <li>{{trans('page.web-developer')}}</li>
                            <li>{{trans('page.app-developer')}}</li>
                            <li>{{trans('page.software-as-service')}}</li>
                            <li>{{trans('page.software-as-business')}}</li>
                        </ul>
                    </div>
                    <a href="{{route('technology')}}" class="btn--big--arrow--round" style="margin-top:25px;">{{trans('page.see-more')}}</a>

                </div>
                <div data-aos="fade-in" data-aos-delay="300" class="col-12 col-xs-4 col-sm-4">
                    <div class="list-element--plus">
                        <p class="md f500">{{trans('page.menu-services-digital-consultant')}}</p>
                        <ul>
                            <li>{{trans('page.technology-adaptation')}}</li>
                            <li>{{trans('page.technology-optimization')}}</li>
                            <li>Performance Management</li>
                            <li>Digital Business Catalyst</li>
                        </ul>
                    </div>
                    <a href="{{route('digital-consultant')}}" class="btn--big--arrow--round" style="margin-top:25px;">{{trans('page.see-more')}}</a>

                </div>
            </div>
        </div>
    </section>
    <!--  END: List Section  -->
    <!--  Video Section  -->
    <section data-bg-top="grey">
        <div class="container">
            <div class="row">
                <div data-aos="fade-up" data-aos-delay="100" class="col-12">
                    <div class="rectangle-element--caption--center--vertical--big">
                        <div class="info">
                            <div class="image">
                                <img data-unveil data-src="{{url('/')}}/images/home/video-yokesen.png" data-src-retina="{{url('/')}}/images/home/video-yokesen.png" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant">
                                <div class="icon--xl--white--shadow--round--white"><i class="fas fa-play"></i></div>
                            </div>
                        </div>
                        <a href="javscript:void(0)" class="link yt-video" data-videoID="KJvS7PSmYnQ"></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END: Video Section  -->
    <!--  Portfolio  -->
    <section data-aos="fade" data-padding>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--inline align-items-md-end">
                        <div class="wrapper">
                            <span class="toptitle">{{trans('page.menu-portfolios')}}</span>
                            <h2>{{trans('page.featured-work')}}</h2>
                        </div>
                        <!--  Portfolio Nav  -->
                        <div class="wrapper portfolio-wrap">
                            <div class="navigation-element--border--round--light">
                                <div class="navigation justify-content-md-end">
                                    <div class="nav--prev">
                                        <i class="feather icon-arrow-left"></i>
                                    </div>
                                    <div class="nav--next">
                                        <i class="feather icon-arrow-right"></i>
                                    </div>
                                    <a href="{{route('portfolios')}}" class="btn--big--round">{{trans('page.view-all')}}</a>
                                </div>
                            </div>
                        </div>
                        <!--  END: Portfolio Nav  -->
                    </div>
                </div>
            </div>
            <div class="row" data-padding="xstop">
                <div class="col-12">
                    <div class="portfolio-carousel owl-carousel">
                        @foreach($portfolios as $data )
                        <div class="square-element--project">
                            <div class="info">
                                <div class="image">
                                    <img class="owl-lazy" data-src="{{url('/').'/'.$data->image_url}}" data-src-retina="{{url('/').'/'.$data->image_url}}" alt="{{$data->name}}">
                                    <!-- <div class="icon--br--square--secondary"><i class="feather icon-plus"></i></div> -->
                                </div>
                                <div class="text">
                                    <h6>{{$data->name}}</h6>
                                    <p>{{$data->tags_service}}</p>
                                </div>
                            </div>
                            <a href="{{route('detail-portfolio',['portfolio_slug'=>$data->portfolio_slug])}}" class="link"></a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END: Portfolio  -->
    <!--  Testimonial Section  -->
    <section data-padding data-bg="template">
        <div class="container">
            <div class="row align-items-md-center">
                <div data-aos="fade" class="col-md-6">
                    <div class="icon-element--testimonial">
                        <div class="info">
                            <div class="text">
                                <p style="font-size: 1rem;">“{{trans('page.greating_ceo')}}“
                                </p>
                                <span class="author">Steffani Lule <em class="meta">CEO</em></span>
                                <!-- <div class="image--big"> -->
                                <img class="logo" data-unveil data-src="{{url('/')}}/images/LogoYokosenPojokKanan.png" data-src-retina="{{url('/')}}/images/LogoYokosenPojokKanan.png" alt="Yokesen - Logo" style="max-height: 60px;">
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div data-aos="zoom-in" data-aos-delay="200" class="col-md-5 offset-md-1">
                    <div class="wrapimage-element--square">
                        <img data-unveil data-src="{{url('/')}}/images/teams/stephany_lule.png" data-src-retina="{{url('/')}}/images/teams/stephany_lule.jpeg" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END: Testimonial Section  -->
    <!--  Blog Section  -->
    @if(count($blogs))
    <section data-padding>
        <div class="container">
            <div data-aos="fade" class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--inline align-items-md-end">
                        <div class="wrapper">
                            <span class="toptitle">{{trans('pages.our_activities')}}</span>
                            <h2>{{trans('page.our_activities_update')}}</h2>
                        </div>
                        <div class="wrapper">
                            <a href="{{route('blogs')}}" class="btn--big--round">{{trans('page.view-all')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" data-padding="xstop">
                @foreach($blogs as $key=>$blog)
                <div data-aos="fade" data-aos-delay="{{$key+1}}00" class="col-12 col-md-4">
                    <!--  Single Article  -->
                    <article class="rectangle-element--article--wide">
                        <div class="info">
                            <div class="image">
                                <img data-unveil data-src="{{url('/').'/'.$blog->blogImage1}}" data-src-retina="{{url('/').'/'.$blog->blogImage1}}" alt="{{$blog->blogTitle}}">
                            </div>
                            <div class="text">
                                <div class="meta--image">
                                    <div class="author--round" style="background-color: black;">
                                        <img data-unveil data-src="{{url('/')}}/images/yokesen.png" data-src-retina="{{url('/')}}/images/yokesen.png" alt="Yokesen" style="height: 78% !important;">
                                    </div>
                                    <div class="tags">
                                        <time class="e-date" datetime="2020-07-05T07:55:21+02:00">{{date('d M Y',strtotime($blog->created_at))}}</time>
                                        <ul class="categories">
                                            <ul class="categories">
                                                <?php $keywords = explode(" ", $blogpopular->blogMetaKeywords); ?>
                                                @foreach ($keywords as $nes)
                                                <li><a href="#.">{{$nes}}</a></li>
                                                @endforeach
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                                <h3>{{$blog->blogTitle}}</h3>
                                <p>{{str_limit($blog->blogMetaDescription,50)}}</p>
                                <a href="{{route('blog-detail',['slug'=>$blog->blogSlug])}}" class="simple--underline">{{trans('page.read_all')}}</a>
                            </div>
                        </div>
                        <a href="{{route('blog-detail',['slug'=>$blog->blogSlug])}}" class="link"></a>
                    </article>
                    <!--  END: Single Article  -->
                </div>

                @endforeach

            </div>
        </div>
    </section>
    @endif
    <!--  END: Blog Section  -->
    <!--  CTA Section  -->
    <section data-aos="fade" data-padding="xstop">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ctabox-element--center--round--light">
                        <div class="text">
                            <h2>{{trans('page.home_bottom_section_1')}}</h2>
                            <p>{{trans('page.home_bottom_section_2')}}</p>
                            <a href="{{route('contact')}}" class="btn--big--border--light--round" target="_self">{{trans('page.hire_us')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END: CTA Section  -->
</div>
@endsection