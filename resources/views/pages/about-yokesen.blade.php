@extends('templates.master')
@section('content')
<!--  Page Header  -->
<!--  class: image, round, hfixedlg, hfixedmd, hfixedsm  -->
<div class="page-header--image" data-overlay>
    <div class="imagebg" style="background-image: url({{url('/')}}/images/about_us/Header.png" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant"></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="sectiontitle-element--light">
                    <h5 style="color:white;">Yokesen aim to be</h5>
                    <h1>Decacorn Company</h1>
                    <h5 style="color:white;">In 2025</h5>
                    <br>
                </div>
                <!--  Breadcrumbs  -->
                <div class="list-element--text--breadcrumbs--arrow--light">
                    <ul>
                        <li><a href="{{route('homeagency')}}">{{trans('page.menu-home')}}</a></li>
                        <li>{{trans('page.menu-about')}} Yokesen</li>
                    </ul>
                </div>
                <!--  END Breadcrumbs  -->
            </div>
        </div>
    </div>
</div>
<!--  END Page Header  -->
<!--  Page Content  -->
<div class="page-content">
    <!--  Intro  -->
    <section data-padding>
        <div class="container">
            <div data-aos="fade-up" class="row" data-padding="xstop">
                <div class="col-12 col-lg-6">
                    <div class="sectiontitle-element">
                        <span class="toptitle">{{trans('page.our-mission')}}</span>
                        <br>
                        <h6>{{trans('page.about-our-mission-left')}}</h6>
                    </div>
                </div>
                <div class="col-12 col-lg-6">
                    <ol style="list-style: decimal;">
                        <li style="margin-bottom: 10px;">
                            <p class="text-justify">{{trans('page.about-our-mission-right-1')}}</p>
                        </li>
                        <li style="margin-bottom: 10px;">
                            <p class="text-justify">{{trans('page.about-our-mission-right-2')}}</p>
                        </li>
                        <li style="margin-bottom: 10px;">
                            <p class="text-justify">{{trans('page.about-our-mission-right-3')}}</p>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--  END Intro  -->
    <!--  Full Width Row  -->
    <section class="fullrow-element--image" data-margin="xstop" data-bg="template">
        <div class="image" style="background-image: url({{url('/')}}/images/about_us/home.png);" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant, digital transformation"></div>
        <div class="main-wrapper">
            <div class="inner-wrapper">
                <div class="sectiontitle-element">
                    <h1>{{trans('page.aboutus-para-1')}}</h1>
                    <p>{{trans('page.aboutus-para-2')}}</p>
                    <p>{{trans('page.aboutus-para-3')}}</p>
                    <p><strong style="font-weight: 800;font-style: italic;color: #222222;">{{trans('page.aboutus-para-4')}}</strong></p>
                    <p>{{trans('page.aboutus-para-5')}}</p>
                    <p>{{trans('page.aboutus-para-6')}}</p>
                </div>
            </div>
        </div>
    </section>
    <!--  END Full Width Row  -->
    <!--  Team  -->
    <section data-padding>
        <div class="container">
            <div data-aos="fade" data-padding="xstop" class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--center">
                        <h2>{{trans('page.our-team')}}</h2>
                        <p>{{trans('page.our-team-title')}}</p>
                    </div>
                </div>
            </div>
            <div class="row" data-padding="smtop">
                <!--  Single Team  -->
                @foreach($ourteams as $row)
                <div data-aos="fade-up" data-margin="xsbottom" class="col-12 col-xs-6 col-sm-6 col-md-4 col-lg-3">
                    <div class="square-element--overlay">
                        <div class="info">
                            <div class="image">
                                <img class="lazyload" data-unveil src="#" data-src="{{asset($row->img_url)}}" data-src-retina="{{url('/').'/'.$row->img_url}}" alt="Yokesen, Yokesen Technology, Yokesen Digital Agency, Yokesen Digital Consultant, {{$row->jabatan}}">
                                <div class="social">
                                    <ul>
                                        @if(!empty($row->linked_in))
                                        <li><a href="{{$row->linked_in}}" target="_blank" rel="noreferrer"><i class="fab fa-linkedin-in"></i></a></li>
                                        @endif
                                        @if(!empty($row->twitter))
                                        <li><a href="{{$row->twitter}}" target="_blank" rel="noreferrer"><i class="fab fa-twitter"></i></a></li>
                                        @endif
                                        @if(!empty($row->instagram))
                                        <li><a href="{{$row->instagram}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        @endif
                                        @if(!empty($row->email))
                                        <li><a href="mailto:{{$row->email}}"><i class="fab fa-envelope"></i></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="text">
                                <h6>{{$row->name}}</h6>
                                <p>{{$row->jabatan}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!--  END Team  -->
    <!--  Partners  -->
    <section data-aos="fade" data-padding="bottom" class="aos-init aos-animate">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="sectiontitle-element--center">
                        <h2>{{trans('page.our-partners')}}</h2>
                    </div>
                    <div class="logo-element--center">
                        <ul>
                            @foreach($ourpartners as $row)
                            <li>
                                <a><img data-unveil src="{{$row->link_url}}" data-src="{{url('/').'/'.$row->image_url}}" alt="{{$row->name}}"></a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END Partners  -->
    <!--  CTA Box  -->
    <section data-aos="fade" data-padding="xstop">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="ctabox-element--center--round--light">
                        <div class="text">
                            <h2>{{trans('page.interested-working-with-us')}}</h2>
                            <p>{{trans('page.to-do-what-you-want')}}</p>
                            <a href="mailto:contact@yokesen.com" class="btn--big--border--light--round" target="_self">{{trans('page.contact-now')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  END CTA Box  -->
</div>
@endsection