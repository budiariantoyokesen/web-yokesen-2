<!DOCTYPE html>
@foreach ($altLocalizedUrls as $alt)
<html lang="{{ $alt['locale']=='id'?'en':'id' }}">
@endforeach

<head>
    <meta name="google-site-verification" content="SqSSSMlmQ7BzztYrUEV06WV325JeSPpo7BkSPi_vhcI" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, shrink-to-fit=no" />

    <title>Yokesen</title>
    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow">
    
    <meta name="format-detection" content="telephone=no" />
    <meta name="msapplication-tap-highlight" content="no" />
    <meta name="theme-color" content="#000" />

    <meta name="description" content="Digital & Technology Adviser" />
    <meta name="author" content="Yokesen" />

    <!-- Favicons -->
    <link rel="icon shortcut" href="{{url('/')}}/asset_favicon.png" sizes="16x16" type="image/png" />
    <link rel="icon" href="{{url('/')}}/asset_favicon.png" sizes="32x32" type="image/png" />
    <!-- <link rel="icon" href="{{url('/')}}/img/favicon-48.png" sizes="48x48" type="image/png" />
    <link rel="icon" href="{{url('/')}}/img/favicon-62.png" sizes="62x62" type="image/png" />
    <link rel="icon" href="{{url('/')}}/img/favicon-96.png" sizes="96x96" type="image/png" /> -->

    <!-- Canonical -->
    <link rel="canonical" href="/" />

    <!-- Open Graph -->
    <meta property="og:image" content="{{url('/').'/'}}@yield('og-image')" />
    <meta property="og:title" content="@yield('og-title')" />
    <meta property="og:description" content="@yield('og-description')" />
    <meta property="og:url" content="{{URL::current()}}" />

    <!-- Twitter -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="" />
    <meta name="twitter:creator" content="" />
    <meta name="twitter:url" content="{{URL::current()}}" />
    <meta name="twitter:title" content="@yield('og-title')" />
    <meta name="twitter:description" content="@yield('og-description')" />
    <meta name="twitter:image" content="{{url('/').'/'}}@yield('og-image')" />

    <!-- Custom css -->
    <link rel="stylesheet" href="{{url('/')}}/css/style.css" />
    <link rel="stylesheet" href="{{url('/')}}/css/custome.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @yield('cssonpage')
</head>

<body>
    <!--  Main Wrap  -->
    <div id="main-wrap">

        @include('templates.header')

        @yield('content')

        <!--  Footer  -->
        @include('templates.footer')
        <!--  END Footer  -->
    </div>
    <!--  Main Wrap  -->
    <!--  Scripts  -->
    <script src="{{url('/')}}/js/jquery.min.js "></script>
    @yield('gmaps')
    <script src="{{url('/')}}/js/jquery.unveil.js"></script>
    <script src="{{url('/')}}/js/aos.js"></script>
    <script src="{{url('/')}}/js/swiper.min.js"></script>
    <script src="{{url('/')}}/js/jquery.magnific-popup.min.js"></script>
    <script src="{{url('/')}}/js/owl.carousel.min.js"></script>
    <script src="{{url('/')}}/js/isotope.min.js"></script>
    <script src="{{url('/')}}/js/imagesloaded.min.js"></script>
    <script src="{{url('/')}}/js/carousel.js"></script>
    <script src="{{url('/')}}/js/menu.js"></script>
    <script src="{{url('/')}}/js/elements.js"></script>
    <script src="{{url('/')}}/js/masonry.js"></script>
    <script src="{{url('/')}}/js/lazysizes.min.js"></script>
    <!-- <script src="{{url('/')}}/js/form.js"></script> -->
    <!--  END Scripts  -->
    @yield('jsonpage')
    @include('sweet::alert')
</body>

</html>