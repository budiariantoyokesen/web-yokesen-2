<footer data-aos="fade" id="footer" data-bg="white">
    <div class="container">
        <div class="row main-wrapper">
            <div class="col-12 col-md-4">
                <div class="logo-footer">
                    <!-- -small, -medium, -big -->
                    <div class="image--big">
                        <img data-unveil data-src="{{url('/')}}/images/LogoYokosenPojokKanan.png" data-src-retina="{{url('/')}}/images/LogoYokosenPojokKanan.png" alt="Yokesen">
                    </div>
                    <h4>{!!trans('page.footer-left-text-1')!!}</h4>
                    <p>{!!trans('page.footer-left-text-2')!!}</p>
                </div>
            </div>
            <div class="col-12 col-md-6 offset-md-2">
                <div class="row">
                    <div class="col-3 col-md-3">
                        <div class="list-element--text">
                            <h6>{{trans('page.pages')}}</h6>
                            <ul>
                                <li><a href="{{route('homeagency')}}">{{trans('page.menu-home')}}</a></li>
                                <li><a href="{{route('about-yokesen')}}">{{trans('page.menu-about')}}</a></li>
                                <li><a href="{{route('yokesen-csr')}}">CSR</a></li>
                                <li><a href="{{route('portfolios')}}">{{trans('page.menu-portfolios')}}</a></li>
                                <li><a href="{{route('careers')}}">{{trans('page.menu-careers')}}</a></li>
                                <!-- <li><a href="plans.html">Contact</a></li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="col-3 col-md-3">
                        <div class="list-element--text">
                            <h6>{{trans('page.menu-services')}}</h6>
                            <ul>
                                <li><a href="{{route('digital-agency')}}">{{trans('page.menu-services-digital-agency')}}</a></li>
                                <li><a href="{{route('technology')}}">{{trans('page.menu-services-technology')}}</a></li>
                                <li><a href="{{route('digital-consultant')}}">{{trans('page.menu-services-digital-consultant')}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-md-6">
                        <div class="info-footer">
                            <h6>{{trans('page.menu-contact')}}</h6>
                            <p>
                                <strong>PT. Emka Anugerah Adijaya</strong><br>
                                Ruko Crystal 8 No 18. 
                                Pakualam, Kec. Serpong Utara, Kota Tangerang Selatan, Banten 15320

                                <br> (021) 22301202
                                </p>
                            <div class="social-element--small">
                                <ul>
                                    <li><a rel="noreferrer" href="https://www.facebook.com/Yokesen-Technology-116526403230902" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a rel="noreferrer" href="https://www.linkedin.com/company/zeroconsultant-com" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a rel="noreferrer" href="https://www.instagram.com/yokesen_id/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    <!-- <li><a href="#"><i class="fab fa-twitter"></i></a></li> -->
                                    <li><a href="mailto:contact@yokesen.com"><i class="far fa-envelope"></i></a></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 down border">
                <div class="wrapper justify-content-md-between align-items-md-center">
                    <div class="link">
                        <div class="list-element--text--inline">
                            <!-- <ul>
                                <li><a href="privacy.html">privacy policy</a></li>
                                <li><a href="privacy.html">cookie policy</a></li>
                            </ul> -->
                        </div>
                    </div>
                    <div class="text">
                        <p>© 2020 Yokesen</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>