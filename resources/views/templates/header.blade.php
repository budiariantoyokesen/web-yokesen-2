 <!--  Header & Menu  -->
 <!--  class: fixed, bg, white, transparent  -->
 @php
 if(isset($noheaderimage)){
 $header_class='bg--white--fixed';
 }else{
 $header_class='transparent--fixed';
 }
 @endphp
 <header id="header" class="{{$header_class}}">
     <!--  padding-sm, padding-md, padding-lg  -->
     <div class="wrap-header--padding-sm">
         <nav class="main-navigation">
             <!--  Header Logo  -->
             <!--  class: height-sm, height-md, height-lg  -->
             <div id="logo" class="height-lg">
                 <a class="navbar-brand" href="/">
                     @if(!isset($noheaderimage))
                     <picture class="light">
                         <source srcset="{{url('/')}}/images/LogoYokosenPojokKananWhite.png 2x, {{url('/')}}/images/LogoYokosenPojokKananWhite.png 1x" type="image/png" />
                         <img class="lazyload" src="{{url('/')}}/images/LogoYokosenPojokKananWhite.png" alt="Logo">
                     </picture>
                     @endif
                     <picture class="colored">
                         <source srcset="{{url('/')}}/images/LogoYokosenPojokKanan.png 2x, {{url('/')}}/images/LogoYokosenPojokKanan.png 1x" type="image/png" />
                         <img class="lazyload" src="{{url('/')}}/images/LogoYokosenPojokKanan.png" alt="Logo">
                     </picture>

                 </a>
             </div>
             <!--  END Header Logo  -->
             <!--  Menu  -->
             <?php
                switch ($currentLanguage->name) {
                    case 'English':
                        $switchlang = 'id';
                        $lang = 'en';
                        break;
                    case 'Bahasa':
                        $switchlang = 'en';
                        $lang = 'id';
                        break;
                    default:
                        $switchlang = 'en';
                        $lang = 'en';
                }
                ?>
             <div id="main-menu" aria-expanded="false" role="navigation">

                 <div class="menu-holder" data-hidden="lg">
                     <ul>
                         <!--  dropdown menu  -->
                         <!--  class: shadow, round, icon, bgitems  -->
                         <li>
                             <a href="{{route('homeagency')}}" class="{{Request::route()&&Request::route()->getName()=='homeagency'?'active':''}}">{{trans('page.menu-home')}}</a>
                         </li>
                         <li>
                             <a href="{{route('about-yokesen')}}" class="{{Request::route()&&Request::route()->getName()=='about-yokesen'?'active':''}}">{{trans('page.menu-about')}}</a>
                         </li>
                         <!-- <li class="dropdown-parent--shadow--round--icon--bgitems">
                             <a href="javascript:void(0)">About</a>
                             <div class="dropdown-inner">
                                 <ul>
                                     <li><a href="{{route('about-yokesen')}}">Yokesen</a></li>
                                     <li><a href="{{route('yokesen-csr')}}">CSR</a></li>
                                 </ul>
                             </div>
                         </li> -->
                         <li class="dropdown-parent--shadow--round--icon--bgitems">
                             <a href="javascript:void(0)">{{trans('page.menu-services')}}</a>
                             <div class="dropdown-inner">
                                 <ul>
                                     <li><a href="{{route('digital-agency')}}">{{trans('page.menu-services-digital-agency')}}</a></li>
                                     <li><a href="{{route('technology')}}">{{trans('page.menu-services-technology')}}</a></li>
                                     <li><a href="{{route('digital-consultant')}}">{{trans('page.menu-services-digital-consultant')}}</a></li>
                                 </ul>
                             </div>
                         </li>
                         <li>
                             <a href="{{route('portfolios')}}" class="{{Request::route()&&Request::route()->getName()=='portfolios'?'active':''}}">{{trans('page.menu-portfolios')}}</a>
                         </li>
                         <li>
                             <a href="{{route('blogs')}}" class="{{Request::route()&&Request::route()->getName()=='blogs'?'active':''}}">{{trans('page.menu-blogs')}}</a>
                         </li>
                         <li>
                             <a href="{{route('careers')}}" class="{{Request::route()&&Request::route()->getName()=='careers'?'active':''}}">{{trans('page.menu-careers')}}</a>
                         </li>
                         <li>
                             <a href="{{route('contact')}}" class="{{Request::route()&&Request::route()->getName()=='contact'?'active':''}}">{{trans('page.menu-contact')}}</a>
                         </li>
                     </ul>
                 </div>
                 <div class="cta">
                     <a href="mailto:contact@yokesen.com" class="btn--medium--round--border" target="_self" style="margin-right: 10px;">{{trans('page.menu-hire-now')}}</a>
                     @foreach ($altLocalizedUrls as $alt)
                     <a href="{{ $alt['url'] }}" hreflang="{{ $alt['locale'] }}" class="btn--medium--round--border" data-hidden="lg">
                         <span class="{{$lang=='id'?'':'text-muted'}}">ID</span> | <span class="{{$lang=='en'?'':'text-muted'}}">EN</span>
                     </a>
                     @endforeach

                 </div>
                 <div class="menu-button" data-visible="lg">
                     <div class="icons">
                         <span class="bar bar-1"></span>
                         <span class="bar bar-2"></span>
                         <span class="bar bar-3"></span>
                     </div>
                 </div>
             </div>
             <div id="mobile-menu" data-visible="lg" aria-expanded="false" role="navigation">
                 <!-- class: -bg -->
                 <div class="mobile-wrap">
                     <!-- class: -dotted, -border -->
                     <div class="main-links--border col-12">
                         <div style="margin-top: 24px;">
                         @foreach ($altLocalizedUrls as $alt)
                         <a href="{{ $alt['url'] }}" hreflang="{{ $alt['locale'] }}" class="btn--medium--round--border">
                             <span class="{{$lang=='id'?'':'text-muted'}}">ID</span> | <span class="{{$lang=='en'?'':'text-muted'}}">EN</span>
                         </a>
                         @endforeach
                         </div>
                         
                         <ul>
                             <li>
                                 <a href="{{route('homeagency')}}" class="{{Request::route()&&Request::route()->getName()=='homeagency'?'active':''}}">{{trans('page.menu-home')}}</a>
                             </li>
                             <li>
                                 <a href="{{route('about-yokesen')}}" class="{{Request::route()&&Request::route()->getName()=='about-yokesen'?'active':''}}">{{trans('page.menu-about')}}</a>
                             </li>
                             <li class="dropdown-parent--icon" aria-expanded="false">
                                 <!--  class: active  -->
                                 <a href="javascript:void(0)">{{trans('page.menu-services')}}</a>
                                 <div class="dropdown-inner">
                                     <ul>
                                         <li><a href="{{route('digital-agency')}}">{{trans('page.menu-services-digital-agency')}}</a></li>
                                         <li><a href="{{route('technology')}}">{{trans('page.menu-services-technology')}}</a></li>
                                         <li><a href="{{route('digital-consultant')}}">{{trans('page.menu-services-digital-consultant')}}</a></li>
                                     </ul>
                                 </div>
                             </li>
                             <li>
                                 <a href="{{route('portfolios')}}" class="{{Request::route()&&Request::route()->getName()=='portfolios'?'active':''}}">{{trans('page.menu-portfolios')}}</a>
                             </li>
                             <li>
                                 <a href="{{route('blogs')}}" class="{{Request::route()&&Request::route()->getName()=='blogs'?'active':''}}">{{trans('page.menu-blogs')}}</a>
                             </li>
                             <li>
                                 <a href="{{route('careers')}}" class="{{Request::route()&&Request::route()->getName()=='careers'?'active':''}}">{{trans('page.menu-careers')}}</a>
                             </li>
                             <li>
                                 <a href="{{route('contact')}}" class="{{Request::route()&&Request::route()->getName()=='contact'?'active':''}}">{{trans('page.menu-contact')}}</a>
                             </li>

                         </ul>

                     </div>
                 </div>
             </div>
             <!--  END Menu  -->
         </nav>
     </div>
 </header>
 <!--  END Header & Menu  -->