@extends('templates.master')
@php
$noheaderimage=true;
@endphp

@section('cssonpage')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
<div class="page-header" data-padding>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sectiontitle-element--center">
                    <h1 class="big">Page not found</h1>
                    <p>Oopss. That page doesn’t exist</p>
                    <a href="{{route('homeagency')}}" class="btn--big--round">Return Home</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection