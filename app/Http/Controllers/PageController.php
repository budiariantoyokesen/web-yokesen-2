<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Jenssegers\Agent\Agent;
use Alert;

class PageController extends Controller
{
    //
    public function homeagency()
    {
        $locale = \UriLocalizer::localeFromRequest();
        $portfolios = DB::table('portofolios')->where([
            ['status', '=', 'Active'],
            ['locale', '=', $locale]
        ])->get();
        $blogs = DB::table('blogs')->where([
            ['blogStatus', '=', 'active'],
            ['locale', '=', $locale]
        ])
            ->orderBy('created_at', 'desc')
            ->limit(3)->get();

        return view('pages.index', compact('portfolios', 'blogs'));
    }

    public function aboutyokesen()
    {
        $ourteams = DB::table('our_teams')->where('status', 'Active')->get();
        $ourpartners = DB::table('our_partners')->where('status', 'Active')->get();
        $agent = new Agent();

        return view('pages.about-yokesen', compact('agent', 'ourpartners', 'ourteams'));
    }

    public function aboutyokesencsr()
    {
        return view('pages.about-yokesen-csr');
    }

    public function yokesenwebinar()
    {
        return view('pages.about-yokesen-webinar');
    }

    public function registeryokesenwebinar(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'mail' => 'email',
            'whatsapp' => 'required',
            // 'link_tokopedia' => 'required',
        ]);

        if ($v->fails()) {
            $errors = $v->errors();
            $data = [];
            if ($errors->has('name')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter your name.", 'id' => "#name"
                ];
            }
            if ($errors->has('mail')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter a valid email.", 'id' => "#mail"
                ];
            }
            if ($errors->has('whatsapp')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter your message.", 'id' => "#whatsapp"
                ];
            }
            if ($errors->has('link_tokopedia')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter your message.", 'id' => "#link_tokopedia"
                ];
            }
        }


        $whatsapp = $request->whatsapp;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if ($check_number[0] == '0') {
            foreach ($check_number as $n => $number) {
                if ($n > 0) {
                    if ($check_number[1] == '8') {
                        $new_number .= $number;
                    } else {;
                        $new_number = '-';
                    }
                }
            }
        } else {
            if ($check_number[0] == '8') {
                $new_number = "62" . $whatsapp;
            } elseif ($check_number[0] == '6') {
                $new_number = $whatsapp;
            } elseif ($check_number[0] == '+') {
                foreach ($check_number as $n => $number) {
                    if ($n > 2) {
                        $new_number .= $number;
                    }
                }
            } else {

                $new_number = '-';
            }
        }
        // dd('new_number',$new_number);
        if ($new_number == '-') {
            // Alert::warning('Nomor Whatsapp anda salah!', 'Error')->persistent('Close');
            // return redirect()->back()->withInput();
            $data[] = [
                'info' => 'error', 'msg' => "Nomor Whatsapp anda salah!", 'id' => "#whatsapp"
            ];

            return response()->json([
                'success' => false,
                'code' => 500,
                'errors' => $data
            ]);
        }

        $id = DB::table('register_webinar')->insertGetId(
            [
                'name' => $request->name,
                'email' => $request->mail,
                'whatsapp_no' => $request->whatsapp,
                'link_tokopedia' => $request->link_tokopedia,
            ]
        );
        return response()->json([
            "success" => true,
            "id" => $id,
            "msg" => "Registered successfully"
        ]);
    }

    public function digitalagency()
    {
        $detailServices = DB::table('detail_services')->where([
            ['services', '=', 'Digital Agency'],
            ['status', '=', 'Active']
        ])->get();
        for ($i = 0; $i < count($detailServices); $i++) {
            $data = $detailServices[$i];
            $galerry = DB::table('detail_service_gallery')->where([
                ['detail_services_id', '=', $data->id],
                ['status', '=', 'Active']
            ])->get();
            $detailServices[$i]->galerry = $galerry;
        }
        return view('pages.service-digitalagency', compact('detailServices'));
    }

    public function technology()
    {
        $detailServices = DB::table('detail_services')->where([
            ['services', '=', 'Technology'],
            ['status', '=', 'Active']
        ])->get();
        for ($i = 0; $i < count($detailServices); $i++) {
            $data = $detailServices[$i];
            $galerry = DB::table('detail_service_gallery')->where([
                ['detail_services_id', '=', $data->id],
                ['status', '=', 'Active']
            ])->get();
            $detailServices[$i]->galerry = $galerry;
        }
        // dd($detailServices);
        return view('pages.service-technology', compact('detailServices'));
    }

    public function digitalconsultant()
    {
        $detailServices = DB::table('detail_services')->where([
            ['services', '=', 'Digital Consultant'],
            ['status', '=', 'Active']
        ])->get();
        for ($i = 0; $i < count($detailServices); $i++) {
            $data = $detailServices[$i];
            $galerry = DB::table('detail_service_gallery')->where([
                ['detail_services_id', '=', $data->id],
                ['status', '=', 'Active']
            ])->get();
            $detailServices[$i]->galerry = $galerry;
        }
        return view('pages.service-digitalconsultant', compact('detailServices'));
    }

    public function portfolios()
    {
        $locale = \UriLocalizer::localeFromRequest();
        $portfolios = DB::table('portofolios')->where([
            ['status', '=', 'Active'],
            ['locale', '=', $locale]
        ])->get();
        return view('pages.portfolios', compact('portfolios'));
    }

    public function detailportfolio($portfolio_slug)
    {
        $locale = \UriLocalizer::localeFromRequest();
        $portfolio = DB::table('portofolios')->where([
            ['portfolio_slug', '=', $portfolio_slug],
            ['locale', '=', $locale],
            ['status', '=', 'Active'],
        ])->first();
        if (empty($portfolio)) {
            $portfolio = DB::table('portofolios')->where([
                ['portfolio_slug', '=', $portfolio_slug],
                // ['locale', '=', $locale],
                ['status', '=', 'Active']
            ])->first();
        }

        $related = DB::table('portofolios')
            ->where([
                ['tags_service', 'like', '%' . $portfolio->tags_service . '%'],
                ['locale', '=', $locale],
                ['status', '=', 'Active'],
            ])
            ->get();

        // dd($portfolio,$related);

        return view('pages.portfoliodetail', compact('portfolio', 'related'));
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function careers()
    {
        return view('pages.careers');
    }

    public function sendmessage(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'mail' => 'email',
            'message' => 'required',
        ]);
        if ($v->fails()) {
            $errors = $v->errors();
            $data = [];
            if ($errors->has('name')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter your name.", 'id' => "#name"
                ];
            }
            if ($errors->has('mail')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter a valid name.", 'id' => "#mail"
                ];
            }
            if ($errors->has('message')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter your message.", 'id' => "#message"
                ];
            }
            return response()->json([
                'success' => false,
                'code' => 500,
                'errors' => $data
            ]);
        }

        $id = DB::table('message')->insertGetId(
            [
                'name' => $request->name,
                'email' => $request->mail,
                'message' => $request->message,
            ]
        );
        return response()->json([
            "success" => true,
            "id" => $id,
            "msg" => "Your message has been sent. We will reply soon. Thank you!"
        ]);
    }

    public function blogs()
    {
        $locale = \UriLocalizer::localeFromRequest();
        $blogpopular = DB::table('blogs')->where([
            ['blogStatus', '=', 'active'],
            ['locale', '=', $locale]
        ])
            ->orderBy('created_at', 'desc')->first();
        $newestblogs = DB::table('blogs')->where(
            [
                ['blogStatus', '=', 'active'],
                ['locale', '=', $locale]
            ]
        )
            ->orderBy('created_at', 'desc')->get();
        // dd($blogpopular);
        return view('pages.blog-list', compact('blogpopular', 'newestblogs'));
    }

    public function blog($slug)
    {
        $locale = \UriLocalizer::localeFromRequest();
        $blog = DB::table('blogs')->where([
            ['blogSlug', '=', $slug],
            ['locale', '=', $locale]
        ])
            ->first();
        if (empty($blog)) {
            $blog = DB::table('blogs')->where([
                ['blogSlug', '=', $slug]
            ])
                ->first();
        }
        $moreblogs = DB::table('blogs')
            ->where([
                ['blogSlug', '!=', $slug],
                ['locale', '=', $locale],
                ['blogStatus', '=', 'active']
            ])
            ->orderBy('created_at', 'desc')->limit(3)->get();
        $comments = DB::table('blog_comments')->where([
            ['blog_id', '=', $blog->id],
            ['status', '=', 'Active']
        ])
            ->orderBy('id', 'desc')
            ->get();
        // dd($blog);
        return view('pages.blog-detail', compact('blog', 'moreblogs', 'comments'));
    }

    public function postcommentblog(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email',
            'message' => 'required',
        ]);
        if ($v->fails()) {
            $errors = $v->errors();
            $data = [];
            if ($errors->has('name')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter your name.", 'id' => "#name"
                ];
            }
            if ($errors->has('email')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter a valid email.", 'id' => "#email"
                ];
            }
            if ($errors->has('message')) {
                $data[] = [
                    'info' => 'error', 'msg' => "Please enter your message.", 'id' => "#message"
                ];
            }
            return response()->json([
                'success' => false,
                'code' => 500,
                'errors' => $data
            ]);
        }

        $comment = [
            'blog_id' => $request->blog_id,
            'name' => $request->name,
            'email' => $request->email,
            'message' => $request->message
        ];

        $id = DB::table('blog_comments')->insertGetId($comment);
        return response()->json([
            "success" => true,
            "id" => $id,
            "msg" => "Your message has been sent.Thank you!"
        ]);
    }

    public function event(Request $request, $event_title_slug)
    {
        // $event = DB::table('events')->where('event_title_slug', '=', $event_title_slug)->first();
        // return view('pages.event-detail', compact('event'));
        return redirect()->route('webinar-page', $event_title_slug);
    }

    public function yokesenwebinardetail(Request $request, $event_title_slug)
    {
        $event = DB::table('events')
            ->where('event_title_slug', '=', $event_title_slug)
            ->where('status', '<>', 'In-active')
            ->first();
        $nextevent = DB::table('events')
            ->where('status', '=', 'Draft')
            ->first();
        return view('pages.event-detail', compact('event', 'nextevent'));
    }

    public function eventregister(Request $request)
    {
        $setvalidation = [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'have_online_shop' => 'required'
        ];


        if ($request->have_online_shop == "1") {
            $setvalidation['online_shop_name'] = 'required';
            $setvalidation['ecoomerce'] = 'required';
        }
        $validator = Validator::make($request->all(), $setvalidation);

        if ($validator->fails()) {
            alert()->warning('Harap periksa kembali data yang anda masukkan.', 'Error')->persistent('Close');
            $errors = $validator->errors();
            return redirect()
                ->back()
                ->withErrors($errors, 'error')
                ->withInput();
        }

        $whatsapp = $request->phone;
        $whatsapp = str_replace('-', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace(' ', '', $whatsapp);
        $whatsapp = str_replace('.', '', $whatsapp);
        $whatsapp = preg_replace("/[^0-9.]/", "", $whatsapp);
        $check_number = str_split($whatsapp);
        $new_number = "62";

        if ($check_number[0] == '0') {
            foreach ($check_number as $n => $number) {
                if ($n > 0) {
                    if ($check_number[1] == '8') {
                        $new_number .= $number;
                    } else {;
                        $new_number = '-';
                    }
                }
            }
        } else {
            if ($check_number[0] == '8') {
                $new_number = "62" . $whatsapp;
            } elseif ($check_number[0] == '6') {
                $new_number = $whatsapp;
            } elseif ($check_number[0] == '+') {
                foreach ($check_number as $n => $number) {
                    if ($n > 2) {
                        $new_number .= $number;
                    }
                }
            } else {

                $new_number = '-';
            }
        }
        // dd('new_number',$new_number);
        if ($new_number == '-') {
            alert()->warning('Nomor Whatsapp anda salah!', 'Error')->persistent('Close');
            return redirect()->back()->withInput();
        }

        $datareg = DB::table('register_webinar')
            ->where('event_title_slug', $request->event_title_slug)
            ->where(function ($query) use ($new_number, $request) {
                $query->where('email', $request->email)
                    ->orWhere('whatsapp_no', $new_number);
            })
            ->first();
        
        if (empty($datareg)) {
            $datainsert = [
                'event_title_slug' => $request->event_title_slug,
                'name' => $request->name,
                'whatsapp_no' => $new_number,
                'email' => $request->email,
                'have_online_shop' => $request->have_online_shop,
                'online_shop_name' => $request->online_shop_name,
                'online_shop_others' => $request->online_shop_others
            ];

            if (isset($request->ecoomerce) && count($request->ecoomerce) > 0) {
                $datainsert['ecoomerce_platform'] = json_encode($request->ecoomerce);
            }

            $id = DB::table('register_webinar')->insertGetId($datainsert);
        } else {
            $dataupdate = [
                'event_title_slug' => $request->event_title_slug,
                'name' => $request->name,
                'whatsapp_no' => $new_number,
                'email' => $request->email,
                'have_online_shop' => $request->have_online_shop,
                'online_shop_name' => $request->online_shop_name,
                'online_shop_others' => $request->online_shop_others
            ];
            $affected = DB::table('register_webinar')
                ->where('id', $datareg->id)
                ->update($dataupdate);
        }

        return redirect()->route('event-register-thankyou');
    }

    public function eventregisterthankyou()
    {
        return view('pages.event-register-thankyou');
    }

    public function sitemapxml()
    {
        $portfolios = DB::table('portofolios')->where('status', 'Active')->orderBy('portfolio_slug', 'desc')->get();
        $blogs = DB::table('blogs')->where('blogStatus', 'active')->orderBy('blogSlug', 'desc')->get();
        return response()->view('pages.sitemapxml', compact('portfolios', 'blogs'))->header('Content-Type', 'text/xml');
    }
}
